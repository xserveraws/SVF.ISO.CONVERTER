:================================================================================================================
::===============================================================================================================
:: Code by s1ave77 [frontend script] // [forums.mydigitallife.info]
:: Bash scripts for File download by mkuba50 // [forums.mydigitallife.info]
:: Win 7/10 source ISO decryption and download by mkuba50 // [forums.mydigitallife.info]
:: SVF Provider Enthousiast // [forums.mydigitallife.info]
:================================================================================================================
::===============================================================================================================
@echo off
pushd %~dp0
if NOT "%cd%"=="%cd: =%" (
	call :SpaceTest
    goto :EOF
)
setlocal EnableExtensions
setlocal EnableDelayedExpansion
:================================================================================================================
::===============================================================================================================
pushd %~dp0
::===============================================================================================================
::===============================================================================================================
for /f "tokens=2* delims= " %%a in ('reg query "HKLM\System\CurrentControlSet\Control\Session Manager\Environment" /v "PROCESSOR_ARCHITECTURE"') do if "%%b"=="AMD64" (set vera=x64) else (set vera=x86)
::===============================================================================================================
::===============================================================================================================
set "database1903=database.1903.smrt"
set "databaseLTSC19=database.LTSC19.smrt"
set "databasetb1803=database.tb.1803.smrt"
set "databasetb18091=database.tb.18091.smrt"
set "databasetb18092=database.tb.18092.smrt"
set "databasetb18093=database.tb.18093.smrt"
set "databasetb81=database.tb.81.smrt"
set "aria2c=files\aria2c.exe"
set "smv=smv.exe"
:================================================================================================================
::===============================================================================================================
:================================================================================================================
::===============================================================================================================
:SVFISOMainMenu
for /f %%I in ('powershell -noprofile ^(Get-Host^).UI.RawUI.WindowSize.width') do set "cw=%%I"
call :TITLE
cls
call :TITLE
call :MenuHeader "[HEADER] MAIN MENU [SYSTEM: %vera%]"
echo:
echo [CREDIT] Enthousiast for SVF Repo [forums.mydigitallife.net]
echo [CREDIT] mkuba50 for SVF Download Script [forums.mydigitallife.net]
echo [CREDIT] mkuba50 for Techbench scripts [forums.mydigitallife.net]
echo:
call :MenuFooter
echo:
echo      [M] MY VISUAL STUDIO DOWNLOADS
call :Footer
echo      [T] TECHBENCH DOWNLOAD [Win 8.1/10]
call :Footer
echo      [D] TOOLS MENU [Conversion/EVAL Downloads]
call :Footer
echo      [E] EXIT
echo:
call :MenuFooter
echo:
CHOICE /C MTDE /N /M "[ USER ] YOUR CHOICE ?:"
if %errorlevel%==1 goto:SVFISODownMenu
if %errorlevel%==2 goto:TBISODownload
if %errorlevel%==3 goto:SVFISOToolsMenu
if %errorlevel%==4 goto:EXIT
goto:SVFISOMainMenu
:================================================================================================================
::===============================================================================================================
:================================================================================================================
::===============================================================================================================
:SVFISOToolsMenu
call :TITLE
cls
call :MenuHeader "[HEADER] MAIN MENU [SYSTEM: %vera%]"
echo:
echo [CREDIT] Enthousiast for SVF Repo [forums.mydigitallife.net]
echo [CREDIT] mkuba50 for SVF Download Script [forums.mydigitallife.net]
echo [CREDIT] mkuba50 for Techbench scripts [forums.mydigitallife.net]
echo:
call :MenuFooter
echo:
echo      [C] CREATE SVF/ISO
call :Footer
echo      [D] DOWNLOAD/RESUME SOURCE ISO FILES
call :Footer
echo      [B] BACK
echo:
call :MenuFooter
echo:
CHOICE /C CDB /N /M "[ USER ] YOUR CHOICE ?:"
if %errorlevel%==1 goto:SVFISOCreate
if %errorlevel%==2 goto:SourceISODownloadRefresh
if %errorlevel%==3 goto:SVFISOMainMenu
goto:SVFISOToolsMenu
:================================================================================================================
::===============================================================================================================
:================================================================================================================
::===============================================================================================================
:SVFISODownMenu
call :TITLE
cls
call :MenuHeader "[HEADER] MVS DOWNLOAD MENU [SYSTEM: %vera%]"
echo:
echo [CREDIT] Enthousiast for SVF Repo [forums.mydigitallife.net]
echo [CREDIT] mkuba50 for SVF Download Script [forums.mydigitallife.net]
echo [CREDIT] mkuba50 for Techbench scripts [forums.mydigitallife.net]
echo:
call :MenuFooter
echo:
echo      # 1903 ####################################################
echo:
echo      [0] 18362.30
echo:
echo      # LTSC 2019 ###############################################
echo:
echo      [1] 17763.1
echo:
call :Footer
echo      [B] BACK
echo:
call :MenuFooter
echo:
CHOICE /C 01B /N /M "[ USER ] YOUR CHOICE ?:"
if %errorlevel%==1 (
	set "show=1903"
	set "build=18362.30"
	goto:SVFISOProcess1903
)
if %errorlevel%==2 (
	set "show=LTSC 2019"
	set "build=17763.1"
	goto:SVFISOProcess1903
)
if %errorlevel%==3 goto:SVFISOMainMenu
goto:SVFISODownMenu
:================================================================================================================
::===============================================================================================================
:================================================================================================================
::===============================================================================================================
:================================================================================================================
::===============================================================================================================
::1903 18362.30 PROCESS
:SVFISOProcess1903
pushd %~dp0
::===============================================================================================================
cls
set "dhash="
set "sdhash="
set "type="
call :Header "[HEADER] %show% [%build%] SVF ISO CONVERSION"
CHOICE /C 68 /N /M "[ USER ] x[6]4 or x[8]6 architecture ?:"
if %errorlevel%==1 set "arch=x64"
if %errorlevel%==2 set "arch=x86"
call :Footer
if "%show%"=="LTSC 2019" goto:LTSCJump
CHOICE /C CB /N /M "[ USER ] [C]onsumer or [B]usiness ?:"
if %errorlevel%==1 set "type=consumer"
if %errorlevel%==2 set "type=business"
call :Footer
:LTSCJump
call :LangChoice
if "%build%"=="18362.30" (
	call :DB1903
	for /f "tokens=1,2,3,4,5 delims=|" %%a in ('type "%database1903%" ^| findstr /i "%lang%" ^| findstr /i "%arch%" ^| findstr /i "%type%"') do (
		set "flink=%%d"
		set "fhash=%%b"
		set "fname=%%c"
		set "shash=%%e"
	)
	if "%arch%"=="x86" (
		if "%type%"=="consumer" (
			set "siename=en_windows_10_consumer_editions_version_1903_x86_dvd_b40c5211"
			set "siehash=e9e187ce83a662619ed05b5b783c12f861e18682"
			set "silink=https://archive.org/download/Win_1903/en_windows_10_consumer_editions_version_1903_x86_dvd_b40c5211.iso"
		)
		if "%type%"=="business" (
			set "siename=en_windows_10_consumer_editions_version_1903_x86_dvd_b40c5211"
			set "siehash=e9e187ce83a662619ed05b5b783c12f861e18682"
			set "silink=https://archive.org/download/Win_1903/en_windows_10_consumer_editions_version_1903_x86_dvd_b40c5211.iso"
	))
	if "%arch%"=="x64" (
		if "%type%"=="consumer" (
			set "siename=en_windows_10_consumer_editions_version_1903_x64_dvd_b980e68c"
			set "siehash=344ca92459c23663d5f857c72a7c030f85f19be8"
			set "silink=https://archive.org/download/Win_1903/en_windows_10_consumer_editions_version_1903_x64_dvd_b980e68c.iso"
		)
		if "%type%"=="business" (
			set "siename=en_windows_10_consumer_editions_version_1903_x64_dvd_b980e68c"
			set "siehash=344ca92459c23663d5f857c72a7c030f85f19be8"
			set "silink=https://archive.org/download/Win_1903/en_windows_10_consumer_editions_version_1903_x64_dvd_b980e68c.iso"
)))
if "%build%"=="17763.1" if "%show%"=="LTSC 2019" (
	call :DBLTSC19
	for /f "tokens=1,2,3,4,5 delims=|" %%a in ('type "%databaseLTSC19%" ^| findstr /i "%lang%" ^| findstr /i "%arch%"') do (
		set "flink=%%d"
		set "fhash=%%b"
		set "fname=%%c"
		set "shash=%%e"
	)
	if "%arch%"=="x86" (
		set "siename=17763.1.180914-1434.rs5_release_CLIENT_LTSC_EVAL_x86FRE_en-us"
		set "siehash=b11efabfc38428b69042da841e1cb4b105be359b"
		set "silink=https://software-download.microsoft.com/download/pr/17763.1.180914-1434.rs5_release_CLIENT_LTSC_EVAL_x86FRE_en-us.iso"
	)
	if "%arch%"=="x64" (
		set "siename=17763.1.180914-1434.rs5_release_CLIENT_LTSC_EVAL_x64FRE_en-us"
		set "siehash=fba7f17ffdf018e4cffd49a9819d719d3708cf09"
		set "silink=https://software-download.microsoft.com/download/pr/17763.1.180914-1434.rs5_release_CLIENT_LTSC_EVAL_x64FRE_en-us.iso"
))
cls
call :Header "[HEADER] %show% [%build%] SVF ISO CONVERSION"
echo [ INFO ] Source: %siename%
echo [ INFO ] Hash  : %siehash%
call :Footer
echo [ INFO ] Target: %fname%
echo [ INFO ] Hash  : %fhash%
call :Footer
CHOICE /C SB /N /M "[ USER ] [S]tart or [B]ack ?:"
if %errorlevel%==2 (
	if exist "*.smrt" del /f /q "*.smrt" >nul 2>&1
	goto:SVFISODownMenu
)
::===============================================================================================================
cls
call :Header "[HEADER] %show% [%build%] SVF ISO CONVERSION"
echo [ INFO ] Source: %siename%
echo [ INFO ] Hash  : %siehash%
call :Footer
echo [ INFO ] Downloading Source ISO ^(if not already pesent^).
call :Footer
set "dhash="
if not exist "%fname%.iso" (
	if not exist "%siename%.iso" (
		call :Footer
		call :SourceISODownload "%siehash%", "%siename%", "%silink%"
		call :Footer
	)
	if exist "%siename%.iso" if not defined dhash (
		echo [ INFO ] Source ISO present.
		call :ISOHashCheck "%siehash%", "%siename%"
		set "dhash="
	)
	if "%lang%"=="en-us" if "%type%"=="consumer" (
		goto :USBreak
	)
	if not exist "%fname%.svf" (
		echo [ INFO ] Downloading Target ISO SVF.
		echo [ INFO ] Name  : %fname%
		echo [ INFO ] Hash  : %shash%
		call :Footer
		call :SVFDownload "%fname%", "%shash%", "%flink%"
		call :Footer
	)
	if exist "%fname%.svf" if not defined sdhash (
		echo [ INFO ] SVF present.
		echo [ INFO ] Name  : %fname%
		call :SVFHashCheck "%shash%", "%fname%"
		set "sdhash="
	)
	echo [ INFO ] Creating Target ISO.
	echo [ INFO ] Name  : %fname%
	call :Footer
::===============================================================================================================
	xcopy "files\smv.exe" /s ".\" /Q /Y >nul 2>&1
	smv x %fname%.svf -br .
::===============================================================================================================
	call :Footer
)
call :ISOHashCheck "%fhash%", "%fname%"
:USBreak
if exist "*.txt" del /f /q "*.txt" >nul 2>&1
if exist "*.exe" del /f /q "*.exe" >nul 2>&1
if exist "*.SMRT" del /f /q "*.SMRT" >nul 2>&1
if exist "*.sh" del /f /q "*.sh" >nul 2>&1
if exist "encrypted" rd /s /q "encrypted" >nul 2>&1
pause
goto:SVFISODownMenu
:================================================================================================================
::===============================================================================================================
::TECHBENCH DOWNLOAD
:TBISODownload
pushd %~dp0
::===============================================================================================================
cls
call :Header "[HEADER] TECHBENCH DOWNLOAD"
echo      [1] WINDOWS 8.1 ^(Update 3^)
echo:
echo      [2] WINDOWS 8.1 N ^(Update 3^)
echo:
echo      [3] WINDOWS 10 1803 ^(contains N versions^)
echo:
echo      [4] WINDOWS 10 1809 [17763.1] ^(contains N versions^)
echo:
echo      [5] WINDOWS 10 1809 [17763.107] ^(contains N versions^)
echo:
echo      [6] WINDOWS 10 1809 [17763.379] ^(contains N versions^)
call :Footer
echo      [B] BACK
call :Footer
CHOICE /C 123456B /N /M "[ USER ] YOUR CHOICE ?:"
if %errorlevel%==1 (
	call :DBTB81
	call :Footer
	set "tbwin=Win8.1_"
	set "tbid=52"
)
if %errorlevel%==2 (
	call :DBTB81
	call :Footer
	set "tbwin=Win8.1_Pro_N_"
	set "tbid=55"
)
if %errorlevel%==3 (
	call :DBTB1803
	call :Footer
	set "tbwin=Win10_1803_"
	set "tbid=651"
)
if %errorlevel%==4 (
	call :DBTB18091
	call :Footer
	set "tbwin=Win10_1809_"
	set "tbid=1019"
)
if %errorlevel%==5 (
	call :DBTB18092
	call :Footer
	set "tbwin=Win10_1809Oct_"
	set "tbid=1060"
)
if %errorlevel%==6 (
	call :DBTB18093
	call :Footer
	set "tbwin=Win10_1809Oct_v2_"
	set "tbid=1203"
)
if %errorlevel%==7 goto:SVFISOMainMenu
call :Footer
CHOICE /C 68 /N /M "[ USER ] x[6]4 or x[8]6 architecture ?:"
if %errorlevel%==1 set "tbarch=x64"
if %errorlevel%==2 set "tbarch=x32"
:ServerEssentials
if "%tbwin%"=="Win8.1_Pro_N_" call :LangChoice81N
if "%tbwin%"=="Win8.1_" call :LangChoice81
if "%tbwin%"=="Win10_1803_" call :LangChoiceTB
if "%tbwin%"=="Win10_1809_" call :LangChoiceTB
if "%tbwin%"=="Win10_1809Oct_" call :LangChoiceTB
if "%tbwin%"=="Win10_1809Oct_v2_" call :LangChoiceTB
call :Footer
::===============================================================================================================
cls
call :Header "[HEADER] TECHBENCH DOWNLOAD"
if "%tbwin%"=="Win10_1809_" (
	if "%tbarch%"=="x64" set "stbarch=x64"
	if "%tbarch%"=="x32" set "stbarch=x86"
)
if "%tbwin%"=="Win10_1809Oct_" (
	if "%tbarch%"=="x64" set "stbarch=x64"
	if "%tbarch%"=="x32" set "stbarch=x86"
)
if "%tbwin%"=="Win10_1809Oct_v2_" (
	if "%tbarch%"=="x64" set "stbarch=x64"
	if "%tbarch%"=="x32" set "stbarch=x86"
)
if "%tbwin%"=="Win10_1803_" (
	if "%tbarch%"=="x64" set "stbarch=x64"
	if "%tbarch%"=="x32" set "stbarch=x86"
)
if "%tblang%"=="Korean" if "%tbwin%"=="Win8.1_" (
	set "tbwin=Win8.1_K_"
	set "tbid=61"
)
if "%tblang%"=="Korean" if "%tbwin%"=="Win8.1_Pro_N_" (
	set "tbwin=Win8.1_Pro_KN_"
	set "tbid=62"
)
set "tbfilename=%tbwin%%tblang%_%tbarch%.iso"
if "%tbwin%"=="Win10_1803_" for /f "tokens=1,2,3* delims=|" %%a in ('type "%databasetb1803%" ^| findstr /i "%tblang%" ^| findstr /i "_%stbarch%_"') do set "tbhash=%%b"
if "%tbwin%"=="Win10_1809_" for /f "tokens=1,2,3* delims=|" %%a in ('type "%databasetb18091%" ^| findstr /i "%tblang%" ^| findstr /i "%stbarch%"') do (
	set "tbhash=%%b"
	set "tbsize=%%c"
)
if "%tbwin%"=="Win10_1809Oct_" for /f "tokens=1,2,3* delims=|" %%a in ('type "%databasetb18092%" ^| findstr /i "%tblang%" ^| findstr /i "%stbarch%"') do (
	set "tbhash=%%b"
	set "tbsize=%%c"
)
if "%tbwin%"=="Win10_1809Oct_v2_" for /f "tokens=1,2,3* delims=|" %%a in ('type "%databasetb18093%" ^| findstr /i "%tblang%" ^| findstr /i "%stbarch%"') do (
	set "tbhash=%%b"
	set "tbsize=%%c"
)
if not "%tbwin%"=="Win10_1803_" if not "%tbwin%"=="Win10_1809_" if not "%tbwin%"=="Win10_1809Oct_" if not "%tbwin%"=="Win10_1809Oct_v2_" if not "%tbwin%"=="Win10_1809Oct_" for /f "tokens=1,2,3,4* delims=|" %%a in ('type "%databasetb81%" ^| findstr /i "%tbfilename%"') do (
	set "tbhash=%%b"
	set "tbsize=%%c"
)
call :getLinkShStream
call :Footer
xcopy "files\busybox.exe" /s ".\" /Q /Y >nul 2>&1
xcopy "files\CURL.exe" /s ".\" /Q /Y >nul 2>&1
call :BusyboxTB
pushd %~dp0
set "busyboxtemp=busybox.cmd"
for /f "tokens=*" %%a in ('call %busyboxtemp% "%tbid%", "'%tbfilename%'"') do set "tblink=%%a"
if exist "_temp" rd /s /q "_temp" >nul 2>&1
if exist "busybox.cmd" del /f /q "busybox.cmd" >nul 2>&1
echo [ INFO ] TB ISO: %tbfilename%
echo [ INFO ] Hash  : %tbhash%
if not "%tbwin%"=="Win10_1803_" echo [ INFO ] Size  : %tbsize% GB
call :Footer
CHOICE /C SB /N /M "[ USER ] [S]tart or [B]ack ?:"
if %errorlevel%==2 (
	if exist "*.txt" del /f /q "*.txt" >nul 2>&1
	if exist "*.exe" del /f /q "*.exe" >nul 2>&1
	if exist "*.SMRT" del /f /q "*.SMRT" >nul 2>&1
	if exist "*.sh" del /f /q "*.sh" >nul 2>&1
	goto:TBISODownload
)
::===============================================================================================================
cls
call :Header "[HEADER] TECHBENCH DOWNLOAD"
echo [ INFO ] TB ISO: %tbfilename%
echo [ INFO ] Hash  : %tbhash%
if not "%tbwin%"=="Win10_1803_" if not "%tbwin%"=="17763.107.101029-1455.rs5_release_svc_refresh_SERVERESSENTIALS_OEM_X64FRE_" echo [ INFO ] Size  : %tbsize% GB
if defined tbhash "%aria2c%" -x16 -s16 -d"%cd%" -o"%tbfilename%" --checksum=sha-1=%tbhash% "%tblink%" -R -c --file-allocation=none --check-certificate=false
if not defined tbhash "%aria2c%" -x16 -s16 -d"%cd%" -o"%tbfilename%" "%tblink%" -R -c --file-allocation=none --check-certificate=false
if not !errorlevel!==0 (
	call :Footer
	busybox echo -e "\033[31;1m[ WARN ] Something went wrong!\033[0m"
	call :Footer
	if exist "*.txt" del /f /q "*.txt" >nul 2>&1
	if exist "*.exe" del /f /q "*.exe" >nul 2>&1
	if exist "*.SMRT" del /f /q "*.SMRT" >nul 2>&1
	if exist "*.sh" del /f /q "*.sh" >nul 2>&1
	pause
	goto:TBISODownload
)
call :Footer
if exist "*.txt" del /f /q "*.txt" >nul 2>&1
if exist "*.exe" del /f /q "*.exe" >nul 2>&1
if exist "*.SMRT" del /f /q "*.SMRT" >nul 2>&1
if exist "*.sh" del /f /q "*.sh" >nul 2>&1
pause
goto:TBISODownload
:================================================================================================================
::===============================================================================================================
:: SVF/ISO CREATION
:SVFISOCreate
pushd %~dp0
::===============================================================================================================
cls
call :Header "[HEADER] SVF/ISO CREATION"
echo      [S] CREATE SVF
echo:
echo      [I] CREATE ISO
echo:
call :Footer
echo      [B] BACK
call :Footer
CHOICE /C SIB /N /M "[ USER ] YOUR CHOICE ?:"
if %errorlevel%==1 goto:CreateSVF
if %errorlevel%==2 goto:CreateISO
if %errorlevel%==3 goto:SVFISOToolsMenu
goto:SVFISOCreate
::===============================================================================================================
:CreateSVF
cls
call :Header "[HEADER] SVF CREATION"
echo Enter Source ISO name ^(no extension^)
echo Default: %siname%
call :Footer
set /p siname=Source ISO name ^>
call :Footer
echo Enter Target ISO name ^(no extension^)
echo Default: %tname%
call :Footer
set /p tname=Target ISO name ^>
call :Footer
cls
call :Header "[HEADER] SVF CREATION"
echo [ INFO ] Source: %siname%
echo [  TO  ]
echo [ INFO ] Target: %tname%
call :Footer
CHOICE /C SB /N /M "[ USER ] [S]tart or [B]ack ?:"
if %errorlevel%==2 goto:SVFISOCreate
call :Footer
xcopy "files\smv.exe" /s ".\" /Q /Y >nul 2>&1
smv BuildPatch "%tname%" "%siname%.iso" "%tname%.iso" -nbhashbits 24 -compressratio 49 -sha1 -sha25
if exist "smv.exe" del /f /q "smv.exe" >nul 2>&1
call :Footer
pause
goto:SVFISOCreate
::===============================================================================================================
:CreateISO
cls
call :Header "[HEADER] ISO CREATION"
echo Enter SVF name ^(no extension^)
echo Default: %sname%
call :Footer
set /p sname=SVF name ^>
call :Footer
cls
call :Header "[HEADER] ISO CREATION"
echo [ INFO ] Target: %sname%
call :Footer
CHOICE /C SB /N /M "[ USER ] [S]tart or [B]ack ?:"
if %errorlevel%==2 goto:SVFISOCreate
call :Footer
xcopy "files\smv.exe" /s ".\" /Q /Y >nul 2>&1
smv x %sname%.svf -br .
if exist "smv.exe" del /f /q "smv.exe" >nul 2>&1
call :Footer
pause
goto:SVFISOCreate
:================================================================================================================
::===============================================================================================================
::SOURCE ISO DOWNLOAD
:SourceISODownloadRefresh
pushd %~dp0
::===============================================================================================================
cls
call :Header "[HEADER] SOURCE ISO DOWNLOAD"
echo      [0] 1903
echo:
echo      [1] LTSC 2019
echo:
call :Footer
echo      [B] BACK
call :Footer
CHOICE /C 01B /N /M "[ USER ] YOUR CHOICE ?:"
if %errorlevel%==1 set "build=1903"
if %errorlevel%==2 (
	set "build=17763.1"
	set "show=LTSC 2019"
)
if %errorlevel%==3 goto:SVFISOToolsMenu
cls
call :Header "[HEADER] SOURCE ISO DOWNLOAD"
CHOICE /C 68 /N /M "[ USER ] x[6]4 or x[8]6 architecture ?:"
if %errorlevel%==1 set "arch=x64"
if %errorlevel%==2 set "arch=x86"
call :Footer
if "%build%"=="1903" if "%arch%"=="x86" (
	set "siname=en_windows_10_consumer_editions_version_1903_x86_dvd_b40c5211"
	set "sihash=e9e187ce83a662619ed05b5b783c12f861e18682"
	set "silink=https://archive.org/download/Win_1903/en_windows_10_consumer_editions_version_1903_x86_dvd_b40c5211.iso"
)
if "%build%"=="1903" if "%arch%"=="x64" (
	set "siname=en_windows_10_consumer_editions_version_1903_x64_dvd_b980e68c"
	set "sihash=344ca92459c23663d5f857c72a7c030f85f19be8"
	set "silink=https://archive.org/download/Win_1903/en_windows_10_consumer_editions_version_1903_x64_dvd_b980e68c.iso"
)
if "%build%"=="17763.1" if "%show%"=="LTSC 2019" (
	if "%arch%"=="x86" (
		set "siname=17763.1.180914-1434.rs5_release_CLIENT_LTSC_EVAL_x86FRE_en-us"
		set "sihash=b11efabfc38428b69042da841e1cb4b105be359b"
		set "silink=https://software-download.microsoft.com/download/pr/17763.1.180914-1434.rs5_release_CLIENT_LTSC_EVAL_x86FRE_en-us.iso"
	)
	if "%arch%"=="x64" (
		set "siname=17763.1.180914-1434.rs5_release_CLIENT_LTSC_EVAL_x64FRE_en-us"
		set "sihash=fba7f17ffdf018e4cffd49a9819d719d3708cf09"
		set "silink=https://software-download.microsoft.com/download/pr/17763.1.180914-1434.rs5_release_CLIENT_LTSC_EVAL_x64FRE_en-us.iso"
))
cls
call :Header "[HEADER] SOURCE ISO DOWNLOAD"
echo [ INFO ] Source: %siname%
echo [ INFO ] Hash  : %sihash%
call :Footer
CHOICE /C SB /N /M "[ USER ] [S]tart or [B]ack ?:"
if %errorlevel%==2 goto:SourceISODownloadRefresh
call :Footer
echo [ INFO ] Downloading.
call :Footer
"%aria2c%" -x16 -s16 -d"%cd%" -o"%siname%.iso" --checksum=sha-1=%sihash% "%silink%" -R -c --file-allocation=none --check-certificate=false
call :Footer
pause
goto:SourceISODownloadRefresh
::===============================================================================================================
:================================================================================================================
::===============================================================================================================
::===============================================================================================================
:================================================================================================================
::===============================================================================================================
::EXIT
:EXIT
ENDLOCAL
exit
:================================================================================================================
::===============================================================================================================
::TITLE
:TITLE
title s1ave77s � S-M-R-T SVF ISO CONVERTER � v0.51.01
goto:eof
::===============================================================================================================
::VERSION
:VERSION
set "svfisoconverter=v0.51.01"
goto:eof
:================================================================================================================
::===============================================================================================================
::MENU HEADER
:MenuHeader
call :Graphics
echo:%~1
call :Graphics
goto:eof
:================================================================================================================
::===============================================================================================================
:: HEADER
:Header
call :Graphics
echo:%~1
call :Graphics
echo:
goto:eof
:================================================================================================================
::===============================================================================================================
::MENU FOOTER
:MenuFooter
call :Graphics
goto:eof
:================================================================================================================
::===============================================================================================================
:: FOOTER
:Footer
echo:
call :Graphics
echo:
goto:eof
:================================================================================================================
::===============================================================================================================
:: GRAPHICS
:Graphics
if %cw% geq 150 echo.
if %cw% geq 145 if %cw% lss 150 echo.
if %cw% geq 140 if %cw% lss 145 echo.
if %cw% geq 135 if %cw% lss 140 echo.
if %cw% geq 130 if %cw% lss 135 echo.
if %cw% geq 125 if %cw% lss 130 echo.
if %cw% geq 120 if %cw% lss 125 echo.
if %cw% geq 115 if %cw% lss 120 echo.
if %cw% geq 110 if %cw% lss 115 echo.
if %cw% geq 105 if %cw% lss 110 echo.
if %cw% geq 100 if %cw% lss 105 echo.
if %cw% geq 95 if %cw% lss 100 echo.
if %cw% geq 90 if %cw% lss 95 echo.
if %cw% geq 85 if %cw% lss 90 echo.
if %cw% geq 80 if %cw% lss 85 echo.
if %cw% lss 80 echo.
goto:eof
:================================================================================================================
::===============================================================================================================
:: ARIA SCRIPT
:AriaWrite
>> "aria.txt" (
	echo %~1
	echo 	out=%~2
	echo 	checksum=sha-1=%~3
	echo:
)
goto:eof
:================================================================================================================
::===============================================================================================================
:: ISO HASH CHECK
:ISOHashCheck
echo [ INFO ] Checking ISO Hash.
echo [ INFO ] Hash  : %~1
call :Footer
xcopy "files\busybox.exe" /s ".\" /Q /Y >nul 2>&1
for /f "tokens=1 delims= " %%a in ('busybox.exe sha1sum %~2.iso') do set "dhash=%%a"
if not !dhash! equ %~1 (
	busybox echo -e "\033[31;1m[ WARN ] Hash Mismatch!\033[0m"
	echo [ INFO ] Hash  : !dhash!
	call :Footer
	if exist "*.txt" del /f /q "*.txt" >nul 2>&1
	if exist "*.exe" del /f /q "*.exe" >nul 2>&1
	if exist "*.SMRT" del /f /q "*.SMRT" >nul 2>&1
	if exist "*.sh" del /f /q "*.sh" >nul 2>&1
	pause
	goto:SVFISOMainMenu
)
if !dhash! equ %~1 (
	busybox echo -e "\033[32;1m[ INFO ] ISO Hash matching.\033[0m"
	echo [ INFO ] Hash  : !dhash!
	call :Footer
)
goto:eof
:================================================================================================================
::===============================================================================================================
:: SVF HASH CHECK
:SVFHashCheck
echo [ INFO ] Checking SVF Hash.
echo [ INFO ] Hash  : %~1
call :Footer
xcopy "files\busybox.exe" /s ".\" /Q /Y >nul 2>&1
for /f "tokens=1 delims= " %%a in ('busybox.exe sha1sum %~2.svf') do set "sdhash=%%a"
if not !sdhash! equ %~1 (
	busybox echo -e "\033[31;1m[ WARN ] Hash Mismatch!\033[0m"
	echo [ INFO ] Hash  : !sdhash!
	call :Footer
	if exist "*.txt" del /f /q "*.txt" >nul 2>&1
	if exist "*.exe" del /f /q "*.exe" >nul 2>&1
	if exist "*.SMRT" del /f /q "*.SMRT" >nul 2>&1
	if exist "*.sh" del /f /q "*.sh" >nul 2>&1
	pause
	goto:SVFISOMainMenu
)
if !sdhash! equ %~1 (
	busybox echo -e "\033[32;1m[ INFO ] SVF Hash matching.\033[0m"
	echo [ INFO ] Hash  : !sdhash!
	call :Footer
)
goto:eof
:================================================================================================================
::===============================================================================================================
:: SOURCE ISO DOWNLOAD
:SourceISODownload
echo [ INFO ] Downloading Source ISO.
call :Footer
"%aria2c%" -x16 -s16 -d"%cd%" -o"%~2.iso" --checksum=sha-1=%~1 "%~3" -R -c --file-allocation=none --check-certificate=false
if !errorlevel!==0 set "dhash=%fhash%"
if not !errorlevel!==0 (
	xcopy "files\busybox.exe" /s ".\" /Q /Y >nul 2>&1
	busybox echo -e "\033[31;1m[ WARN ] Hash Mismatch!\033[0m"
	echo [ INFO ] Hash  : !dhash!
	call :Footer
	if exist "*.txt" del /f /q "*.txt" >nul 2>&1
	if exist "*.exe" del /f /q "*.exe" >nul 2>&1
	if exist "*.SMRT" del /f /q "*.SMRT" >nul 2>&1
	if exist "*.sh" del /f /q "*.sh" >nul 2>&1
	pause
	goto:SVFISOMainMenu
)
goto:eof
:================================================================================================================
::===============================================================================================================
:: SVF DOWNLOAD
:SVFDownload
"%aria2c%" -x16 -s16 -d"%cd%" -o"%~1.svf" --checksum=sha-1=%~2 "%~3" -R -c --file-allocation=none --check-certificate=false
if !errorlevel!==0 set "sdhash=%shash%"
if not !errorlevel!==0 (
	xcopy "files\busybox.exe" /s ".\" /Q /Y >nul 2>&1
	busybox echo -e "\033[31;1m[ WARN ] Hash Mismatch!\033[0m"
	echo [ INFO ] Hash  : !shash!
	call :Footer
	if exist "*.txt" del /f /q "*.txt" >nul 2>&1
	if exist "*.exe" del /f /q "*.exe" >nul 2>&1
	if exist "*.SMRT" del /f /q "*.SMRT" >nul 2>&1
	if exist "*.sh" del /f /q "*.sh" >nul 2>&1
	pause
	goto:SVFISOMainMenu
)
goto:eof
:================================================================================================================
::===============================================================================================================
:: DECRYPT ISO
:DecryptISO
if [%1] == [] echo No name of file provided && goto :EOF
set "file=%1"
if exist %file% echo %file% exists && goto :EOF
set "OPENSSL_CONF=%~dp0files\openssl.conf"
set "HOME=%~dp0"
pushd %~dp0
if not exist encrypted echo Directory "encrypted" does not exist && goto :EOF
if not exist encrypted.txt echo encrypted.txt does not exist && goto :EOF
for /F "tokens=1,2,3 delims=|" %%i in (encrypted.txt) do (
    if not exist "encrypted/%%i" echo Part %%i does not exist! Cannot continue. && exit /b 1
    call :DECRYPT_FILE "encrypted/%%i" %%j %%k
)
goto:eof
:================================================================================================================
::===============================================================================================================
:DECRYPT_FILE
if [%1] == [] goto :EOF
if [%2] == [] goto :EOF
if [%3] == [] goto :EOF
echo Decrypting %1...
set "newFile=%file:\=\\%"
"%~dp0files\openssl.exe" aes256 -d -K %2 -iv %3 -in "%1" | "%~dp0files\busybox" ash -c "cat >>"%newFile%""
goto:eof
:================================================================================================================
::===============================================================================================================
:: DECRYPT ISO NEW
:DecryptISONew
if [%1] == [] echo No name of file provided && goto :EOF
set "file=%1"
if exist %file% echo %file% exists && goto :EOF
set "OPENSSL_CONF=%~dp0files\openssl.conf"
set "HOME=%~dp0"
pushd %~dp0
if not exist encrypted echo Directory "encrypted" does not exist && goto :EOF
if not exist datainfo.txt echo datainfo.txt does not exist && goto :EOF
set "sha1="
set "computedSha1="
for /F "tokens=* delims=" %%i in (datainfo.txt) do set "%%i"
echo Preparing files...
for %%i in (encrypted\%name%.*.obf) do (
    echo Preparing %%~nxi...
rem #For some arbitrary reason known only by Microsoft the variable does not
rem #want to load here, so I had to work around this shit this way.
    call :GET_DATA_FOR_FILE "%%i"
    if "[!FILE_DATA_RESPONSE!]" == "[]" echo An error has occurred & exit /b 1
    echo !FILE_DATA_RESPONSE! | "%~dp0files\busybox.exe" base64 -d | "%~dp0files\busybox.exe" dd of="%%i" bs=64 count=1 conv=notrunc 2>NUL
    if %errorlevel% NEQ 0 echo An error has occurred & exit /b 1
    rename "%%i" "%%~ni" >NUL
)
::We have to test the archive before unpacking, to see if it is correct,
::because pipes do not give errorlevel sent by 7-Zip on fail.
echo.
echo Testing archive...
"%~dp0files\7za.exe" -p%pass% t encrypted\%name%.001
if %errorlevel% NEQ 0 echo An error has occurred & exit /b 1
echo.
echo Unpacking and decrypting data...
"%~dp0files\7za.exe" -p%pass% -bso2 -bsp2 -so e encrypted\%name%.001 %name% | "%~dp0files\openssl.exe" camellia256 -d -K %key% -iv %iv% -out "%file%"
echo.
goto:eof
::===============================================================================================================
::===============================================================================================================
:GET_DATA_FOR_FILE
if "[%1]" == "[]" goto :EOF
set "FILE_DATA_RESPONSE=!file_%~n1!"
goto:eof
:================================================================================================================
::===============================================================================================================
:: ARIA WIN 7SCRIPT
:GenerateList
pushd %~dp0
if not exist download.txt echo download.txt does not exist && goto :EOF
if exist aria2.txt del aria2.txt
for /F "tokens=1,2,3 delims=|" %%i in (download.txt) do call :GEN_ARIA2 %%i %%j "%%k"
goto:eof
:================================================================================================================
::===============================================================================================================
:GEN_ARIA2
if [%1] == [] goto :EOF
if [%2] == [] goto :EOF
if [%3] == [] goto :EOF
set "link="
set "link=%3"
set "link=%link:"=%"
echo %link%>>aria2.txt
echo   out=%1>>aria2.txt
echo   checksum=sha-1=%2>>aria2.txt
echo.>>aria2.txt
goto:eof
:================================================================================================================
::===============================================================================================================
:: LANGUAGE CHOICE NON-N
:LangChoice
echo Enter chosen language Number.
echo:
echo Available:
echo:
echo [01] ar-sa = Arabic [Saudi Arabia]
echo [02] bg-bg = Bulgarian [Bulgaria]
echo [03] cs-cz = Czech [Czech Republic]
echo [04] da-dk = Danish [Denmark]
echo [05] de-de = German [Germany]
echo [06] el-gr = Greek [Greece]
echo [07] en-gb = English [United Kingdom]
echo [08] en-us = English [United States]
echo [09] es-es = Spanish [Spain]
echo [10] es-mx = Spanish [Mexico]
echo [11] et-ee = Estonian [Estonia]
echo [12] fi-fi = Finnish [Finland]
echo [13] fr-ca = French [Canada]
echo [14] fr-fr = French [France]
echo [15] he-il = Hebrew [Israel]
echo [16] hr-hr = Croatian [Croatia]
echo [17] hu-hu = Hungarian [Hungary]
echo [18] it-it = Italian [Italy]
echo [19] ja-jp = Japanese [Japan]
echo [20] ko-kr = Korean [Korea]
echo [21] lt-lt = Lithuanian [Lithuania]
echo [22] lv-lv = Latvian [Latvia]
echo [23] nb-no = Norwegian [Norway]
echo [24] nl-nl = Dutch [Netherlands]
echo [25] pl-pl = Polish [Poland]
echo [26] pt-br = Portuguese [Brazil]
echo [27] pt-pt = Portuguese [Portugal]
echo [28] ro-ro = Romanian [Romania]
echo [29] ru-ru = Russian [Russia]
echo [30] sr-latn-rs = Serbian [Serbia]
echo [31] sk-sk = Slovak [Slovakia]
echo [32] sl-si = Slovenian [Slovenia]
echo [33] sv-se = Swedish [Sweden]
echo [34] th-th = Thai [Thailand]
echo [35] tr-tr = Turkish [Turkey]
echo [36] uk-ua = Ukrainian [Ukraine]
echo [37] zh-cn = Chinese [PRC]
echo [38] zh-tw = Chinese [Taiwan]
call :Footer
CHOICE /C 0123 /N /M "[ USER ] Enter Digit One:"
if %errorlevel%==1 set "number=0"
if %errorlevel%==2 set "number=10"
if %errorlevel%==3 set "number=20"
if %errorlevel%==4 set "number=30"
call :Footer
CHOICE /C 1234567890 /N /M "[ USER ] Enter Digit Two:"
if %errorlevel%==1 set /a number+=1
if %errorlevel%==2 set /a number+=2
if %errorlevel%==3 set /a number+=3
if %errorlevel%==4 set /a number+=4
if %errorlevel%==5 set /a number+=5
if %errorlevel%==6 set /a number+=6
if %errorlevel%==7 set /a number+=7
if %errorlevel%==8 set /a number+=8
if %errorlevel%==9 set /a number+=9
if %errorlevel%==10 set /a number+=0
if %number%==1 set "lang=ar-sa"
if %number%==2 set "lang=bg-bg"
if %number%==3 set "lang=cs-cz"
if %number%==4 set "lang=da-dk"
if %number%==5 set "lang=de-de"
if %number%==6 set "lang=el-gr"
if %number%==7 set "lang=en-gb"
if %number%==8 set "lang=en-us"
if %number%==9 set "lang=es-es"
if %number%==10 set "lang=es-mx"
if %number%==11 set "lang=et-ee"
if %number%==12 set "lang=fi-fi"
if %number%==13 set "lang=fr-ca"
if %number%==14 set "lang=fr-fr"
if %number%==15 set "lang=he-il"
if %number%==16 set "lang=hr-hr"
if %number%==17 set "lang=hu-hu"
if %number%==18 set "lang=it-it"
if %number%==19 set "lang=ja-jp"
if %number%==20 set "lang=ko-kr"
if %number%==21 set "lang=lt-lt"
if %number%==22 set "lang=lv-lv"
if %number%==23 set "lang=nb-no"
if %number%==24 set "lang=nl-nl"
if %number%==25 set "lang=pl-pl"
if %number%==26 set "lang=pt-br"
if %number%==27 set "lang=pt-pt"
if %number%==28 set "lang=ro-ro"
if %number%==29 set "lang=ru-ru"
if %number%==30 set "lang=sr-latn-rs"
if %number%==31 set "lang=sk-sk"
if %number%==32 set "lang=sl-si"
if %number%==33 set "lang=sv-se"
if %number%==34 set "lang=th-th"
if %number%==35 set "lang=tr-tr"
if %number%==36 set "lang=uk-ua"
if %number%==37 set "lang=zh-cn"
if %number%==38 set "lang=zh-tw"
goto:eof
:================================================================================================================
::===============================================================================================================
:: LANGUAGE CHOICE TECHBENCH 10
:LangChoiceTB
echo Enter chosen language Number.
echo:
echo Available:
echo:
echo [01] ar-sa = Arabic [Saudi Arabia]
echo [02] bg-bg = Bulgarian [Bulgaria]
echo [03] cs-cz = Czech [Czech Republic]
echo [04] da-dk = Danish [Denmark]
echo [05] de-de = German [Germany]
echo [06] el-gr = Greek [Greece]
echo [07] en-gb = English [United Kingdom]
echo [08] en-us = English [United States]
echo [09] es-es = Spanish [Spain]
echo [10] es-mx = Spanish [Mexico]
echo [11] et-ee = Estonian [Estonia]
echo [12] fi-fi = Finnish [Finland]
echo [13] fr-ca = French [Canada]
echo [14] fr-fr = French [France]
echo [15] he-il = Hebrew [Israel]
echo [16] hr-hr = Croatian [Croatia]
echo [17] hu-hu = Hungarian [Hungary]
echo [18] it-it = Italian [Italy]
echo [19] ja-jp = Japanese [Japan]
echo [20] ko-kr = Korean [Korea]
echo [21] lt-lt = Lithuanian [Lithuania]
echo [22] lv-lv = Latvian [Latvia]
echo [23] nb-no = Norwegian [Norway]
echo [24] nl-nl = Dutch [Netherlands]
echo [25] pl-pl = Polish [Poland]
echo [26] pt-br = Portuguese [Brazil]
echo [27] pt-pt = Portuguese [Portugal]
echo [28] ro-ro = Romanian [Romania]
echo [29] ru-ru = Russian [Russia]
echo [30] sr-latn-rs = Serbian [Serbia]
echo [31] sk-sk = Slovak [Slovakia]
echo [32] sl-si = Slovenian [Slovenia]
echo [33] sv-se = Swedish [Sweden]
echo [34] th-th = Thai [Thailand]
echo [35] tr-tr = Turkish [Turkey]
echo [36] uk-ua = Ukrainian [Ukraine]
echo [37] zh-cn = Chinese [PRC]
echo [38] zh-tw = Chinese [Taiwan]
call :Footer
CHOICE /C 0123 /N /M "[ USER ] Enter Digit One:"
if %errorlevel%==1 set "number=0"
if %errorlevel%==2 set "number=10"
if %errorlevel%==3 set "number=20"
if %errorlevel%==4 set "number=30"
call :Footer
CHOICE /C 1234567890 /N /M "[ USER ] Enter Digit Two:"
if %errorlevel%==1 set /a number+=1
if %errorlevel%==2 set /a number+=2
if %errorlevel%==3 set /a number+=3
if %errorlevel%==4 set /a number+=4
if %errorlevel%==5 set /a number+=5
if %errorlevel%==6 set /a number+=6
if %errorlevel%==7 set /a number+=7
if %errorlevel%==8 set /a number+=8
if %errorlevel%==9 set /a number+=9
if %errorlevel%==10 set /a number+=0
if %number%==1 set "tblang=Arabic"
if %number%==2 set "tblang=Bulgarian"
if %number%==3 set "tblang=Czech"
if %number%==4 set "tblang=Danish"
if %number%==5 set "tblang=German"
if %number%==6 set "tblang=Greek"
if %number%==7 set "tblang=EnglishInternational"
if %number%==8 set "tblang=English"
if %number%==9 set "tblang=Spanish"
if %number%==10 set "tblang=Spanish(Mexico)"
if %number%==11 set "tblang=Estonian"
if %number%==12 set "tblang=Finnish"
if %number%==13 set "tblang=FrenchCanadian"
if %number%==14 set "tblang=French"
if %number%==15 set "tblang=Hebrew"
if %number%==16 set "tblang=Croatian"
if %number%==17 set "tblang=Hungarian"
if %number%==18 set "tblang=Italian"
if %number%==19 set "tblang=Japanese"
if %number%==20 set "tblang=Korean"
if %number%==21 set "tblang=Lithuanian"
if %number%==22 set "tblang=Latvian"
if %number%==23 set "tblang=Norwegian"
if %number%==24 set "tblang=Dutch"
if %number%==25 set "tblang=Polish"
if %number%==26 set "tblang=BrazilianPortuguese"
if %number%==27 set "tblang=Portuguese"
if %number%==28 set "tblang=Romanian"
if %number%==29 set "tblang=Russian"
if %number%==30 set "tblang=SerbianLatin"
if %number%==31 set "tblang=Slovak"
if %number%==32 set "tblang=Slovenian"
if %number%==33 set "tblang=Swedish"
if %number%==34 set "tblang=Thai"
if %number%==35 set "tblang=Turkish"
if %number%==36 set "tblang=Ukrainian"
if %number%==37 set "tblang=Chinese(Simplified)"
if %number%==38 set "tblang=Chinese(Traditional)"
goto:eof
:================================================================================================================
::===============================================================================================================
:: LANGUAGE CHOICE VLSC
:LangChoiceVLSC
echo Enter chosen language Number.
echo:
echo Available:
echo:
echo [01] ar-sa = Arabic [Saudi Arabia]
echo [02] bg-bg = Bulgarian [Bulgaria]
echo [03] cs-cz = Czech [Czech Republic]
echo [04] da-dk = Danish [Denmark]
echo [05] de-de = German [Germany]
echo [06] el-gr = Greek [Greece]
echo [07] en-gb = English [United Kingdom]
echo [08] en-us = English [United States]
echo [09] es-es = Spanish [Spain]
echo [10] es-mx = Spanish [Mexico]
echo [11] et-ee = Estonian [Estonia]
echo [12] fi-fi = Finnish [Finland]
echo [13] fr-ca = French [Canada]
echo [14] fr-fr = French [France]
echo [15] he-il = Hebrew [Israel]
echo [16] hr-hr = Croatian [Croatia]
echo [17] hu-hu = Hungarian [Hungary]
echo [18] it-it = Italian [Italy]
echo [19] ja-jp = Japanese [Japan]
echo [20] ko-kr = Korean [Korea]
echo [21] lt-lt = Lithuanian [Lithuania]
echo [22] lv-lv = Latvian [Latvia]
echo [23] nb-no = Norwegian [Norway]
echo [24] nl-nl = Dutch [Netherlands]
echo [25] pl-pl = Polish [Poland]
echo [26] pt-br = Portuguese [Brazil]
echo [27] pt-pt = Portuguese [Portugal]
echo [28] ro-ro = Romanian [Romania]
echo [29] ru-ru = Russian [Russia]
echo [30] sr-latn-rs = Serbian [Serbia]
echo [31] sk-sk = Slovak [Slovakia]
echo [32] sl-si = Slovenian [Slovenia]
echo [33] sv-se = Swedish [Sweden]
echo [34] th-th = Thai [Thailand]
echo [35] tr-tr = Turkish [Turkey]
echo [36] uk-ua = Ukrainian [Ukraine]
echo [37] zh-cn = Chinese [PRC]
echo [38] zh-tw = Chinese [Taiwan]
call :Footer
CHOICE /C 0123 /N /M "[ USER ] Enter Digit One:"
if %errorlevel%==1 set "number=0"
if %errorlevel%==2 set "number=10"
if %errorlevel%==3 set "number=20"
if %errorlevel%==4 set "number=30"
call :Footer
CHOICE /C 1234567890 /N /M "[ USER ] Enter Digit Two:"
if %errorlevel%==1 set /a number+=1
if %errorlevel%==2 set /a number+=2
if %errorlevel%==3 set /a number+=3
if %errorlevel%==4 set /a number+=4
if %errorlevel%==5 set /a number+=5
if %errorlevel%==6 set /a number+=6
if %errorlevel%==7 set /a number+=7
if %errorlevel%==8 set /a number+=8
if %errorlevel%==9 set /a number+=9
if %errorlevel%==10 set /a number+=0
if %number%==1 set "tblang=Arabic"
if %number%==2 set "tblang=Bulgarian"
if %number%==3 set "tblang=Czech"
if %number%==4 set "tblang=Danish"
if %number%==5 set "tblang=German"
if %number%==6 set "tblang=Greek"
if %number%==7 set "tblang=English_International"
if %number%==8 set "tblang=English_MLF"
if %number%==9 set "tblang=Spanish_MLF"
if %number%==10 set "tblang=Spanish_Latam"
if %number%==11 set "tblang=Estonian"
if %number%==12 set "tblang=Finnish"
if %number%==13 set "tblang=French_Canadian"
if %number%==14 set "tblang=French_MLF"
if %number%==15 set "tblang=Hebrew"
if %number%==16 set "tblang=Croatian"
if %number%==17 set "tblang=Hungarian"
if %number%==18 set "tblang=Italian"
if %number%==19 set "tblang=Japanese"
if %number%==20 set "tblang=Korean"
if %number%==21 set "tblang=Lithuanian"
if %number%==22 set "tblang=Latvian"
if %number%==23 set "tblang=Norwegian"
if %number%==24 set "tblang=Dutch"
if %number%==25 set "tblang=Polish"
if %number%==26 set "tblang=Brazilian_MLF"
if %number%==27 set "tblang=Portuguese_MLF"
if %number%==28 set "tblang=Romanian"
if %number%==29 set "tblang=Russian"
if %number%==30 set "tblang=Serbian_Latin"
if %number%==31 set "tblang=Slovak"
if %number%==32 set "tblang=Slovenian"
if %number%==33 set "tblang=Swedish"
if %number%==34 set "tblang=Thai"
if %number%==35 set "tblang=Turkish"
if %number%==36 set "tblang=Ukrainian"
if %number%==37 set "tblang=Chinese_Simplified"
if %number%==38 set "tblang=Chinese_Traditional"
goto:eof
:================================================================================================================
::===============================================================================================================
:: LANGUAGE CHOICE TB 8.1
:LangChoice81
echo Enter chosen language Number.
echo:
echo Available:
echo:
echo [01] ar-sa = Arabic [Saudi Arabia]
echo [02] bg-bg = Bulgarian [Bulgaria]
echo [03] cs-cz = Czech [Czech Republic]
echo [04] da-dk = Danish [Denmark]
echo [05] de-de = German [Germany]
echo [06] el-gr = Greek [Greece]
echo [07] en-gb = English [United Kingdom]
echo [08] en-us = English [United States]
echo [09] es-es = Spanish [Spain]
echo [10] et-ee = Estonian [Estonia]
echo [11] fi-fi = Finnish [Finland]
echo [12] fr-fr = French [France]
echo [13] he-il = Hebrew [Israel]
echo [14] hr-hr = Croatian [Croatia]
echo [15] hu-hu = Hungarian [Hungary]
echo [16] it-it = Italian [Italy]
echo [17] ja-jp = Japanese [Japan]
echo [18] ko-kr = Korean [Korea]
echo [19] lt-lt = Lithuanian [Lithuania]
echo [20] lv-lv = Latvian [Latvia]
echo [21] nb-no = Norwegian [Norway]
echo [22] nl-nl = Dutch [Netherlands]
echo [23] pl-pl = Polish [Poland]
echo [24] pt-br = Portuguese [Brazil]
echo [25] pt-pt = Portuguese [Portugal]
echo [26] ro-ro = Romanian [Romania]
echo [27] ru-ru = Russian [Russia]
echo [28] sr-latn-rs = Serbian [Serbia]
echo [29] sk-sk = Slovak [Slovakia]
echo [30] sl-si = Slovenian [Slovenia]
echo [31] sv-se = Swedish [Sweden]
echo [32] th-th = Thai [Thailand]
echo [33] tr-tr = Turkish [Turkey]
echo [34] uk-ua = Ukrainian [Ukraine]
echo [35] zh-cn = Chinese [PRC]
echo [36] zh-tw = Chinese [Taiwan]
echo [37] zh-hk = Chinese [Hong Kong]
call :Footer
CHOICE /C 0123 /N /M "[ USER ] Enter Digit One:"
if %errorlevel%==1 set "number=0"
if %errorlevel%==2 set "number=10"
if %errorlevel%==3 set "number=20"
if %errorlevel%==4 set "number=30"
call :Footer
CHOICE /C 1234567890 /N /M "[ USER ] Enter Digit Two:"
if %errorlevel%==1 set /a number+=1
if %errorlevel%==2 set /a number+=2
if %errorlevel%==3 set /a number+=3
if %errorlevel%==4 set /a number+=4
if %errorlevel%==5 set /a number+=5
if %errorlevel%==6 set /a number+=6
if %errorlevel%==7 set /a number+=7
if %errorlevel%==8 set /a number+=8
if %errorlevel%==9 set /a number+=9
if %errorlevel%==10 set /a number+=0
if %number%==1 set "tblang=Arabic"
if %number%==2 set "tblang=Bulgarian"
if %number%==3 set "tblang=Czech"
if %number%==4 set "tblang=Danish"
if %number%==5 set "tblang=German"
if %number%==6 set "tblang=Greek"
if %number%==7 set "tblang=EnglishInternational"
if %number%==8 set "tblang=English"
if %number%==9 set "tblang=Spanish"
if %number%==10 set "tblang=Estonian"
if %number%==11 set "tblang=Finnish"
if %number%==12 set "tblang=French"
if %number%==13 set "tblang=Hebrew"
if %number%==14 set "tblang=Croatian"
if %number%==15 set "tblang=Hungarian"
if %number%==16 set "tblang=Italian"
if %number%==17 set "tblang=Japanese"
if %number%==18 set "tblang=Korean"
if %number%==19 set "tblang=Lithuanian"
if %number%==20 set "tblang=Latvian"
if %number%==21 set "tblang=Norwegian"
if %number%==22 set "tblang=Dutch"
if %number%==23 set "tblang=Polish"
if %number%==24 set "tblang=BrazilianPortuguese"
if %number%==25 set "tblang=Portuguese"
if %number%==26 set "tblang=Romanian"
if %number%==27 set "tblang=Russian"
if %number%==28 set "tblang=SerbianLatin"
if %number%==29 set "tblang=Slovak"
if %number%==30 set "tblang=Slovenian"
if %number%==31 set "tblang=Swedish"
if %number%==32 set "tblang=Thai"
if %number%==33 set "tblang=Turkish"
if %number%==34 set "tblang=Ukrainian"
if %number%==35 set "tblang=Chinese(Simplified)"
if %number%==36 set "tblang=Chinese(Traditional)"
if %number%==37 set "tblang=Chinese(TraditionalHongKong)"
goto:eof
:================================================================================================================
::===============================================================================================================
:: LANGUAGE CHOICE TB 8.1 N
:LangChoice81N
echo Enter chosen language Number.
echo:
echo Available:
echo:
echo [01] bg-bg = Bulgarian [Bulgaria]
echo [02] cs-cz = Czech [Czech Republic]
echo [03] da-dk = Danish [Denmark]
echo [04] de-de = German [Germany]
echo [05] el-gr = Greek [Greece]
echo [06] en-gb = English [United Kingdom]
echo [07] en-us = English [United States]
echo [08] es-es = Spanish [Spain]
echo [09] et-ee = Estonian [Estonia]
echo [10] fi-fi = Finnish [Finland]
echo [11] fr-fr = French [France]
echo [12] hr-hr = Croatian [Croatia]
echo [13] hu-hu = Hungarian [Hungary]
echo [14] it-it = Italian [Italy]
echo [15] ko-kr = Korean [Korea]
echo [16] lt-lt = Lithuanian [Lithuania]
echo [17] lv-lv = Latvian [Latvia]
echo [18] nb-no = Norwegian [Norway]
echo [19] nl-nl = Dutch [Netherlands]
echo [20] pl-pl = Polish [Poland]
echo [21] pt-pt = Portuguese [Portugal]
echo [22] ro-ro = Romanian [Romania]
echo [23] sk-sk = Slovak [Slovakia]
echo [24] sl-si = Slovenian [Slovenia]
echo [25] sv-se = Swedish [Sweden]
call :Footer
CHOICE /C 012 /N /M "[ USER ] Enter Digit One:"
if %errorlevel%==1 set "number=0"
if %errorlevel%==2 set "number=10"
if %errorlevel%==3 set "number=20"
call :Footer
CHOICE /C 1234567890 /N /M "[ USER ] Enter Digit Two:"
if %errorlevel%==1 set /a number+=1
if %errorlevel%==2 set /a number+=2
if %errorlevel%==3 set /a number+=3
if %errorlevel%==4 set /a number+=4
if %errorlevel%==5 set /a number+=5
if %errorlevel%==6 set /a number+=6
if %errorlevel%==7 set /a number+=7
if %errorlevel%==8 set /a number+=8
if %errorlevel%==9 set /a number+=9
if %errorlevel%==10 set /a number+=0
if %number%==1 set "tblang=Bulgarian"
if %number%==2 set "tblang=Czech"
if %number%==3 set "tblang=Danish"
if %number%==4 set "tblang=German"
if %number%==5 set "tblang=Greek"
if %number%==6 set "tblang=EnglishInternational"
if %number%==7 set "tblang=English"
if %number%==8 set "tblang=Spanish"
if %number%==9 set "tblang=Estonian"
if %number%==10 set "tblang=Finnish"
if %number%==11 set "tblang=French"
if %number%==12 set "tblang=Croatian"
if %number%==13 set "tblang=Hungarian"
if %number%==14 set "tblang=Italian"
if %number%==15 set "tblang=Korean"
if %number%==16 set "tblang=Lithuanian"
if %number%==17 set "tblang=Latvian"
if %number%==18 set "tblang=Norwegian"
if %number%==19 set "tblang=Dutch"
if %number%==20 set "tblang=Polish"
if %number%==21 set "tblang=Portuguese"
if %number%==22 set "tblang=Romanian"
if %number%==23 set "tblang=Slovak"
if %number%==24 set "tblang=Slovenian"
if %number%==25 set "tblang=Swedish"
goto:eof
:================================================================================================================
::===============================================================================================================
:: LANGUAGE CHOICE SERVER 2016
:LangChoiceS
echo Enter chosen language Number.
echo:
echo Available:
echo:
echo [01] cs-cz = Czech [Czech Republic]
echo [02] de-de = German [Germany]
echo [03] en-us = English [United States]
echo [04] es-es = Spanish [Spain]
echo [05] fr-fr = French [France]
echo [06] hu-hu = Hungarian [Hungary]
echo [07] it-it = Italian [Italy]
echo [08] ja-jp = Japanese [Japan]
echo [09] ko-kr = Korean [Korea]
echo [10] nl-nl = Dutch [Netherlands]
echo [11] pl-pl = Polish [Poland]
echo [12] pt-br = Portuguese [Brazil]
echo [13] pt-pt = Portuguese [Portugal]
echo [14] ru-ru = Russian [Russia]
echo [15] sv-se = Swedish [Sweden]
echo [16] tr-tr = Turkish [Turkey]
echo [17] zh-cn = Chinese [PRC]
echo [18] zh-tw = Chinese [Taiwan]
call :Footer
CHOICE /C 01 /N /M "[ USER ] Enter Digit One:"
if %errorlevel%==1 set "number=0"
if %errorlevel%==2 set "number=10"
call :Footer
CHOICE /C 1234567890 /N /M "[ USER ] Enter Digit Two:"
if %errorlevel%==1 set /a number+=1
if %errorlevel%==2 set /a number+=2
if %errorlevel%==3 set /a number+=3
if %errorlevel%==4 set /a number+=4
if %errorlevel%==5 set /a number+=5
if %errorlevel%==6 set /a number+=6
if %errorlevel%==7 set /a number+=7
if %errorlevel%==8 set /a number+=8
if %errorlevel%==9 set /a number+=9
if %errorlevel%==10 set /a number+=0
if %number%==1 set "lang=cs-cz"
if %number%==2 set "lang=de-de"
if %number%==3 set "lang=en-us"
if %number%==4 set "lang=es-es"
if %number%==5 set "lang=fr-fr"
if %number%==6 set "lang=hu-hu"
if %number%==7 set "lang=it-it"
if %number%==8 set "lang=ja-jp"
if %number%==9 set "lang=ko-kr"
if %number%==10 set "lang=nl-nl"
if %number%==11 set "lang=pl-pl"
if %number%==12 set "lang=pt-br"
if %number%==13 set "lang=pt-pt"
if %number%==14 set "lang=ru-ru"
if %number%==15 set "lang=sv-se"
if %number%==16 set "lang=tr-tr"
if %number%==17 set "lang=zh-cn"
if %number%==18 set "lang=zh-tw"
goto:eof
:================================================================================================================
::===============================================================================================================
:: LANGUAGE CHOICE N
:LangChoiceN
echo Enter chosen language Number.
echo:
echo Available:
echo:
echo [01] bg-bg = Bulgarian [Bulgaria]
echo [02] cs-cz = Czech [Czech Republic]
echo [03] da-dk = Danish [Denmark]
echo [04] de-de = German [Germany]
echo [05] el-gr = Greek [Greece]
echo [06] en-gb = English [United Kingdom]
echo [07] en-us = English [United States]
echo [08] es-es = Spanish [Spain]
echo [09] et-ee = Estonian [Estonia]
echo [10] fi-fi = Finnish [Finland]
echo [11] fr-fr = French [France]
echo [12] hr-hr = Croatian [Croatia]
echo [13] hu-hu = Hungarian [Hungary]
echo [14] it-it = Italian [Italy]
echo [15] lt-lt = Lithuanian [Lithuania]
echo [16] lv-lv = Latvian [Latvia]
echo [17] nb-no = Norwegian [Norway]
echo [18] nl-nl = Dutch [Netherlands]
echo [19] pl-pl = Polish [Poland]
echo [20] pt-pt = Portuguese [Portugal]
echo [21] ro-ro = Romanian [Romania]
echo [22] sk-sk = Slovak [Slovakia]
echo [23] sl-si = Slovenian [Slovenia]
echo [24] sv-se = Swedish [Sweden]
call :Footer
CHOICE /C 012 /N /M "[ USER ] Enter Digit One:"
if %errorlevel%==1 set "number=0"
if %errorlevel%==2 set "number=10"
if %errorlevel%==3 set "number=20"
call :Footer
CHOICE /C 1234567890 /N /M "[ USER ] Enter Digit Two:"
if %errorlevel%==1 set /a number+=1
if %errorlevel%==2 set /a number+=2
if %errorlevel%==3 set /a number+=3
if %errorlevel%==4 set /a number+=4
if %errorlevel%==5 set /a number+=5
if %errorlevel%==6 set /a number+=6
if %errorlevel%==7 set /a number+=7
if %errorlevel%==8 set /a number+=8
if %errorlevel%==9 set /a number+=9
if %errorlevel%==10 set /a number+=0
if %number%==1 set "lang=bg-bg"
if %number%==2 set "lang=cs-cz"
if %number%==3 set "lang=da-dk"
if %number%==4 set "lang=de-de"
if %number%==5 set "lang=el-gr"
if %number%==6 set "lang=en-gb"
if %number%==7 set "lang=en-us"
if %number%==8 set "lang=es-es"
if %number%==9 set "lang=et-ee"
if %number%==10 set "lang=fi-fi"
if %number%==11 set "lang=fr-fr"
if %number%==12 set "lang=hr-hr"
if %number%==13 set "lang=hu-hu"
if %number%==14 set "lang=it-it"
if %number%==15 set "lang=lt-lt"
if %number%==16 set "lang=lv-lv"
if %number%==17 set "lang=nb-no"
if %number%==18 set "lang=nl-nl"
if %number%==19 set "lang=pl-pl"
if %number%==20 set "lang=pt-pt"
if %number%==21 set "lang=ro-ro"
if %number%==22 set "lang=sk-sk"
if %number%==23 set "lang=sl-si"
if %number%==24 set "lang=sv-se"
goto:eof
:================================================================================================================
::===============================================================================================================
:BusyBox
pushd %~dp0
cd /d "%~dp0"
if [%~2]==[] if [%~3]==[] (
	busybox ash -c "./download.sh "%~1""
)
if not [%~2]==[] if [%~3]==[] (
	busybox ash -c "./download.sh "%~1" "%~2""
)

if not [%~3]==[] if [%~5]==[] (
	if [%~3]==[x86] if [%~4]==[edition_version] busybox ash -c "./download.sh "%~1" "EN2XX%%20Multi%%20CLIENT%%20x86%%20SVF/%~2""
	if [%~3]==[x64] if [%~4]==[edition_version] busybox ash -c "./download.sh "%~1" "EN2XX%%20Multi%%20CLIENT%%20x64%%20SVF/%~2""
	if [%~3]==[x86] if [%~4]==[edition_vl_version] busybox ash -c "./download.sh "%~1" "EN2XX%%20Multi%%20VOLUME%%20x86%%20SVF/%~2""
	if [%~3]==[x64] if [%~4]==[edition_vl_version] busybox ash -c "./download.sh "%~1" "EN2XX%%20Multi%%20VOLUME%%20x64%%20SVF/%~2""
)
if not [%~3]==[] if [%~5]==[16299.15] (
	if [%~3]==[x86] if [%~4]==[edition_version] busybox ash -c "./download.sh "%~1" "EN2XX%%20Multi%%20Client/EN2XX%%20x86%%20Multi%%20Client%%20SVF/%~2""
	if [%~3]==[x64] if [%~4]==[edition_version] busybox ash -c "./download.sh "%~1" "EN2XX%%20Multi%%20Client/EN2XX%%20x64%%20Multi%%20Client%%20SVF/%~2""
	if [%~3]==[x86] if [%~4]==[edition_vl_version] busybox ash -c "./download.sh "%~1" "EN2XX%%20Multi%%20VOLUME/EN2XX%%20x86%%20Multi%%20VOLUME%%20SVF/%~2""
	if [%~3]==[x64] if [%~4]==[edition_vl_version] busybox ash -c "./download.sh "%~1" "EN2XX%%20Multi%%20VOLUME/EN2XX%%20x64%%20Multi%%20VOLUME%%20SVF/%~2""
)
goto:eof
:================================================================================================================
::===============================================================================================================
:BusyBoxTB
>>busybox.cmd (
echo @echo off
echo pushd %%~dp0
echo cd /d "%%~dp0"
echo busybox ash -c "./getLink.sh "%%~1" "%%~2""
echo goto:eof
)
goto:eof
:================================================================================================================
::===============================================================================================================
:SpaceTest
	echo:
	echo ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	echo:
    echo Current directory contains spaces in its path.
	echo:
    echo Please move or rename the directory to one not containing spaces.
	echo:
	echo ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	echo:
    pause
goto:eof
:================================================================================================================
:================================================================================================================
:================================================================================================================
:================================================================================================================
:================================================================================================================
::DBTB1803 STREAMLINED
::===============================================================================================================
::===============================================================================================================
:DBTB1803
>>database.tb.1803.smrt (
echo Arabic^|5db2b4bee4f68ec5377d277281bbaa23cfab46fa^|ar_windows_10_consumer_editions_version_1803_updated_march_2018_x64_dvd_12064254
echo Arabic^|78ed4931032348a3748c0b2d40a588d6ec2b6cf6^|ar_windows_10_consumer_editions_version_1803_updated_march_2018_x86_dvd_12063370
echo Bulgarian^|fdf9c914611e4f7065fc8b55a7bd839a111d4790^|bg_windows_10_consumer_editions_version_1803_updated_march_2018_x64_dvd_12064334
echo Bulgarian^|6a495230b4d83823c471ee8b02a78c16d6bb369b^|bg_windows_10_consumer_editions_version_1803_updated_march_2018_x86_dvd_12063372
echo Chinese^(Simplified^)^|82091d67fff5b49726ffc22d35d9c1cbe81dc443^|cn_windows_10_consumer_editions_version_1803_updated_march_2018_x64_dvd_12063766
echo Chinese^(Simplified^)^|d5e89fb54169f5a2e610c75813ce833a4cb9a4e4^|cn_windows_10_consumer_editions_version_1803_updated_march_2018_x86_dvd_12063452
echo Czech^|06c5e10c969b03b99734f4b5c5ee9ec571e2f96f^|cs_windows_10_consumer_editions_version_1803_updated_march_2018_x64_dvd_12063734
echo Czech^|f80db91e842f276a6d0ade5c723ccadcd994be84^|cs_windows_10_consumer_editions_version_1803_updated_march_2018_x86_dvd_12063373
echo Chinese^(Traditional^)^|db56ca8415c6f597df7cb916c958180fca549e21^|ct_windows_10_consumer_editions_version_1803_updated_march_2018_x64_dvd_12063769
echo Chinese^(Traditional^)^|dad11416c8bd69f0e855990eff2bca2bd01a090a^|ct_windows_10_consumer_editions_version_1803_updated_march_2018_x86_dvd_12063453
echo Danish^|e1e957d3bd8eb4014da244f6c85f8975da602632^|da_windows_10_consumer_editions_version_1803_updated_march_2018_x64_dvd_12063737
echo Danish^|1f95a80a3f40b9c8576dca91946e54776ff2b730^|da_windows_10_consumer_editions_version_1803_updated_march_2018_x86_dvd_12063374
echo German^|537f817703323d28f673b4e36e377919e1ee034e^|de_windows_10_consumer_editions_version_1803_updated_march_2018_x64_dvd_12063739
echo German^|2440c7c38a8342f6c075fd17685e86da3fd5d40c^|de_windows_10_consumer_editions_version_1803_updated_march_2018_x86_dvd_12063375
echo Greek^|79422a3e1cef23e09c5eaf41d70a6f86e70f4392^|el_windows_10_consumer_editions_version_1803_updated_march_2018_x64_dvd_12064255
echo Greek^|981ad20ee8e630e8a392fd73c497626d8e6a5152^|el_windows_10_consumer_editions_version_1803_updated_march_2018_x86_dvd_12063381
echo EnglishInternational^|2f8910ce2cfaf61f094dfd49fe87ac004a6200af^|en-gb_windows_10_consumer_editions_version_1803_updated_march_2018_x64_dvd_12063738
echo EnglishInternational^|e12e5d860b73e14424503372eee39895a55cad9a^|en-gb_windows_10_consumer_editions_version_1803_updated_march_2018_x86_dvd_12063377
echo English^|08fbb24627fa768f869c09f44c5d6c1e53a57a6f^|en_windows_10_consumer_editions_version_1803_updated_march_2018_x64_dvd_12063379
echo English^|3f2063b7419675e98c8df82bc0607bbb1ce298bb^|en_windows_10_consumer_editions_version_1803_updated_march_2018_x86_dvd_12063380
echo Spanish^(Mexico^)^|412421bb1cea4e373849f66b4073303103861e0b^|es-mx_windows_10_consumer_editions_version_1803_updated_march_2018_x64_dvd_12063741
echo Spanish^(Mexico^)^|be534b157c38b2ccc627009c4e80689c21fefd3d^|es-mx_windows_10_consumer_editions_version_1803_updated_march_2018_x86_dvd_12063413
echo Spanish^|8909d21adcdb2ee15a48cdd941711b351133f87d^|es_windows_10_consumer_editions_version_1803_updated_march_2018_x64_dvd_12064257
echo Spanish^|9f5df1360cb0b4fc4a8ecb8517f80fbd0ae5f28d^|es_windows_10_consumer_editions_version_1803_updated_march_2018_x86_dvd_12063390
echo Estonian^|c35128538bc2def7bbdd170f0e429f9419392b6b^|et_windows_10_consumer_editions_version_1803_updated_march_2018_x64_dvd_12063740
echo Estonian^|a2ba3f0e28bc04713a58df0b8853bbe3ed689cf7^|et_windows_10_consumer_editions_version_1803_updated_march_2018_x86_dvd_12063415
echo Finnish^|f62635feb63904cfb588c1faac0b9fdbbd9f517d^|fi_windows_10_consumer_editions_version_1803_updated_march_2018_x64_dvd_12063746
echo Finnish^|39f0058d814d76135bfc3c9c128482fdf4b57468^|fi_windows_10_consumer_editions_version_1803_updated_march_2018_x86_dvd_12064463
echo FrenchCanadian^|7b38cf3de9e1cbda7a4880314dfc92dc5a969aaf^|fr-ca_windows_10_consumer_editions_version_1803_updated_march_2018_x64_dvd_12064258
echo FrenchCanadian^|ec4d16e74ab99b8736fae10020f709da76439cfb^|fr-ca_windows_10_consumer_editions_version_1803_updated_march_2018_x86_dvd_12064465
echo French^|62564786f5568b40385abb40a1a9bb889f3576c7^|fr_windows_10_consumer_editions_version_1803_updated_march_2018_x64_dvd_12064260
echo French^|730d7834aa90b3e3ffa0a8fb0d0144a3a7b1c43c^|fr_windows_10_consumer_editions_version_1803_updated_march_2018_x86_dvd_12064466
echo Hebrew^|38fa10c7054416e5189017dc712a2c5104167d87^|he_windows_10_consumer_editions_version_1803_updated_march_2018_x64_dvd_12063750
echo Hebrew^|ce63c53e0edd21eae59e271b93c096e7327d686a^|he_windows_10_consumer_editions_version_1803_updated_march_2018_x86_dvd_12063422
echo Croatian^|ee8d311750ba2be4d9b078f1e2ef0c682e9aafd4^|hr_windows_10_consumer_editions_version_1803_updated_march_2018_x64_dvd_12063751
echo Croatian^|f95a65fc6b9d3944748e5f71b903dd690aae5489^|hr_windows_10_consumer_editions_version_1803_updated_march_2018_x86_dvd_12063423
echo Hungarian^|2c9d96591073fab7a1464e1af8ca7c74a865848c^|hu_windows_10_consumer_editions_version_1803_updated_march_2018_x64_dvd_12064263
echo Hungarian^|5396aeb43c37f5c9eed8d9954e9230056884cfa7^|hu_windows_10_consumer_editions_version_1803_updated_march_2018_x86_dvd_12063425
echo Italian^|2226a27470aadd3897963a6b14b1a82ab2766918^|it_windows_10_consumer_editions_version_1803_updated_march_2018_x64_dvd_12064265
echo Italian^|84af39d466955ee9ed09988ed6b45e8a59049dbe^|it_windows_10_consumer_editions_version_1803_updated_march_2018_x86_dvd_12063426
echo Japanese^|a3dad3fe7886aaf33dd950191eed776e4ee79717^|ja_windows_10_consumer_editions_version_1803_updated_march_2018_x64_dvd_12064266
echo Japanese^|1dc3154c7213239e3629893a08954f5141e9a733^|ja_windows_10_consumer_editions_version_1803_updated_march_2018_x86_dvd_12063427
echo Korean^|83b391c8d0238a37c880197ab3720c1d064a9f61^|ko_windows_10_consumer_editions_version_1803_updated_march_2018_x64_dvd_12064336
echo Korean^|74a9b0cd58fac42d80e8728dd70dbe9aa6d73545^|ko_windows_10_consumer_editions_version_1803_updated_march_2018_x86_dvd_12064467
echo Lithuanian^|1f160da0a2b51b732b57c03a7a97e05db795e459^|lt_windows_10_consumer_editions_version_1803_updated_march_2018_x64_dvd_12064269
echo Lithuanian^|5e7ef1d3b6a27bfd492b55d9427ca5b57c4e4b21^|lt_windows_10_consumer_editions_version_1803_updated_march_2018_x86_dvd_12063432
echo Latvian^|6faa8c8640fd4f76d1a1a0a5e687e3238610e4b3^|lv_windows_10_consumer_editions_version_1803_updated_march_2018_x64_dvd_12063754
echo Latvian^|683c6fe93c389ffb79652a4e0c185be8b910929c^|lv_windows_10_consumer_editions_version_1803_updated_march_2018_x86_dvd_12063433
echo Norwegian^|5e093a98a3787a85a2d7330edc1c6bf91b13207d^|nb_windows_10_consumer_editions_version_1803_updated_march_2018_x64_dvd_12063756
echo Norwegian^|ecf9dc1030f064f27a4b4594c771a3e19fb410af^|nb_windows_10_consumer_editions_version_1803_updated_march_2018_x86_dvd_12063434
echo Dutch^|1eaafb03d19a87bc955843d81693897bf4156f26^|nl_windows_10_consumer_editions_version_1803_updated_march_2018_x64_dvd_12063757
echo Dutch^|fb4b1e6f643814bf51e5678f62597385031b2f90^|nl_windows_10_consumer_editions_version_1803_updated_march_2018_x86_dvd_12063435
echo Polish^|3e097b4ee1c011845f72963ecbbe96afd6d9494f^|pl_windows_10_consumer_editions_version_1803_updated_march_2018_x64_dvd_12063758
echo Polish^|1e88c8d6ca678e43395e64a3e24115429526a120^|pl_windows_10_consumer_editions_version_1803_updated_march_2018_x86_dvd_12063437
echo Portuguese^|76aa50207e1927fca80b0d8a2311d075551745b1^|pp_windows_10_consumer_editions_version_1803_updated_march_2018_x64_dvd_12064735
echo Portuguese^|f4b3a3157a3224a29ec9db7608ab3aa7b7bf436d^|pp_windows_10_consumer_editions_version_1803_updated_march_2018_x86_dvd_12063438
echo BrazilianPortuguese^|7c855dcc37d1c40631f84c830b382bc4934fdb2f^|pt_windows_10_consumer_editions_version_1803_updated_march_2018_x64_dvd_12064271
echo BrazilianPortuguese^|527a581da2bcdfd3ad8788ee288890a4a21c5e30^|pt_windows_10_consumer_editions_version_1803_updated_march_2018_x86_dvd_12063436
echo Romanian^|01bf23d8a277ef4d6c207b38b9493695e2828dbc^|ro_windows_10_consumer_editions_version_1803_updated_march_2018_x64_dvd_12064276
echo Romanian^|8180b61a6a83231fe2aaeb2efacc241d2a9dcc22^|ro_windows_10_consumer_editions_version_1803_updated_march_2018_x86_dvd_12063439
echo Russian^|21758e6046c5adc3b485219aeebc46ed93ae68a4^|ru_windows_10_consumer_editions_version_1803_updated_march_2018_x64_dvd_12064277
echo Russian^|6b6f75060d4a18761951a533143af5145772d839^|ru_windows_10_consumer_editions_version_1803_updated_march_2018_x86_dvd_12064736
echo Slovak^|7ffd9ae5ab2893e0ce59664ab6099513d8607418^|sk_windows_10_consumer_editions_version_1803_updated_march_2018_x64_dvd_12064278
echo Slovak^|734086f531c8b0670af73d1dd8276940c71db89d^|sk_windows_10_consumer_editions_version_1803_updated_march_2018_x86_dvd_12063442
echo Slovenian^|61e1b5beee0d002f6ad2dbc77991ab2867dfe956^|sl_windows_10_consumer_editions_version_1803_updated_march_2018_x64_dvd_12064280
echo Slovenian^|d4f321695e4644e16b70a855a3f64a8b252886db^|sl_windows_10_consumer_editions_version_1803_updated_march_2018_x86_dvd_12063445
echo SerbianLatin^|ad6d807aad81ef4e06bcc98d96bbbf92d5391e92^|sr-latn_windows_10_consumer_editions_version_1803_updated_march_2018_x64_dvd_12064281
echo SerbianLatin^|5b6acb34a43fe02cb95b6f1a6636a03f8e679d8b^|sr-latn_windows_10_consumer_editions_version_1803_updated_march_2018_x86_dvd_12063448
echo Swedish^|9ce38a862b067c1ab445591f4d0f01d137982cee^|sv_windows_10_consumer_editions_version_1803_updated_march_2018_x64_dvd_12064284
echo Swedish^|ee45fe5c914415c71671838acfda9f18b17dee9b^|sv_windows_10_consumer_editions_version_1803_updated_march_2018_x86_dvd_12064475
echo Thai^|4e42a197dc55b06cb5ac10a173d5403786bb8cab^|th_windows_10_consumer_editions_version_1803_updated_march_2018_x64_dvd_12063760
echo Thai^|662095cf81d861f8b19f0311886359b394019abe^|th_windows_10_consumer_editions_version_1803_updated_march_2018_x86_dvd_12064474
echo Turkish^|37ff7a7a649db4fd3b528762ec3eec7a51488bd6^|tr_windows_10_consumer_editions_version_1803_updated_march_2018_x64_dvd_12063761
echo Turkish^|2274a98571e93b1bcd1d4368f766b579a3cbd116^|tr_windows_10_consumer_editions_version_1803_updated_march_2018_x86_dvd_12064476
echo Ukrainian^|aa53674d84e5beeaf4cab80329aa9f996b3b4738^|uk_windows_10_consumer_editions_version_1803_updated_march_2018_x64_dvd_12063765
echo Ukrainian^|49aef067ffc76fe6d2c059e1519851e6369a3b85^|uk_windows_10_consumer_editions_version_1803_updated_march_2018_x86_dvd_12063451
)
goto:eof
:================================================================================================================
:================================================================================================================
:================================================================================================================
:================================================================================================================
:================================================================================================================
::DBTB18091 STREAMLINED
::===============================================================================================================
::===============================================================================================================
:DBTB18091
>>database.tb.18091.smrt (
echo Arabic^|29eec618b904e2549463f68d55092b27a16c1dbe^|4.22^|x64
echo BrazilianPortuguese^|68d8827b66165a87f4b73712273ed48c086^|4.22^|x64
echo Bulgarian^|16e3d66b8cfdf772cd4dccaf79cf6710198401a1^|4.36^|x64
echo Chinese^(Simplified^)^|c8deade18822435d8d47011609c124496e94df71^|4.48^|x64
echo Chinese^(Traditional^)^|a6b5d9bdccaa19ead31b781e23aa94aabe864d6f^|4.44^|x64
echo Croatian^|16461ad737586b08cb1e89f6ef0d9324e59c6612^|4.33^|x64
echo Czech^|18b8cbaf977ab99b87ba4d740906a41484d77e40^|4.35^|x64
echo Danish^|e58468cc62f96987d4d14ab6bd0f25fb21972cc2^|4.38^|x64
echo Dutch^|ee5fbe989ed9c770c69176cef9d3c29dee25a5e0^|4.36^|x64
echo EnglishInternational^|84e64722300c855a25a177514d72f1d1cb8efd3b^|4.47^|x64
echo English^|500c9635645a91c09d3c3904fd12f0e1fd3f709e^|4.47^|x64
echo Estonian^|b67d900982b5beee735dfefbbb07132d4eed6f4c^|4.32^|x64
echo Finnish^|1f5c32c96236aadfb6e69328835fdb9e332d9d51^|4.36^|x64
echo FrenchCanadian^|0ec328877d153784321b97b2ba40e2ddb3c43387^|4.22^|x64
echo French^|9754ccb2fa7f293bc9dcadad16a2222c5576b566^|4.49^|x64
echo German^|26bc9a1a5c08d9e8be886cd12fe4ff0eabd1c9c6^|4.49^|x64
echo Greek^|99fa31c341263b3344926df8b664009f68920d84^|4.38^|x64
echo Hebrew^|7991d0d72ab487b8aab03af1dc378d127331ec15^|4.18^|x64
echo Hungarian^|4937b804ac67eaa319c759d647103c20dc665c50^|4.35^|x64
echo Italian^|6cf89c376d3c3d65749cc45bc3925d42960496d5^|4.41^|x64
echo Japanese^|4709a94c64c144207f184b103810898d71d5b99b^|4.42^|x64
echo Korean^|67b0412e2f1b6b2753c1c5287eb718a64ea7fee1^|4.23^|x64
echo Latvian^|0f6e35cd3d4d5fd2f964cefe7b28dfef3625d9f7^|4.33^|x64
echo Lithuanian^|04c3d73b655f91bfd353ac07fe7cee18b9da51b0^|4.33^|x64
echo Norwegian^|2ddd9e1b5635d8d486480501dab6704b02ca8f52^|4.35^|x64
echo Polish^|96bbe53246ff389764502dc4f503118b83246131^|4.38^|x64
echo Portuguese^|e08cd41d78f2031eb4e09363574ec738cc47a25e^|4.4^|x64
echo Romanian^|862fc5619b0e3932bc9c72ea53eb54b908990320^|4.33^|x64
echo Russian^|f905f71f6f75a0aec99ff1cfc091cb46bac5f6fc^|4.23^|x64
echo SerbianLatin^|6e1f5ed604eec8f298fc7806952cbfbd287151fd^|4.14^|x64
echo Slovak^|bf1913aecbdb1ad18c1fd6c338ef55f220cb1144^|4.34^|x64
echo Slovenian^|54030c95fdb2bddd788948e2ac62e1fdc14e37b2^|4.34^|x64
echo Spanish^(Mexico^)^|942b12c8791102913a296be4156e23a6611172aa^|4.22^|x64
echo Spanish^|431ccaf191e34049043e6d3c0cf8b424ad1b0e3c^|4.49^|x64
echo Swedish^|37945d6986457ea8d3df02cbb56dece66b13b1ec^|4.36^|x64
echo Thai^|63411ed63997a012fefa94cc97ab495db466d6c0^|4.17^|x64
echo Turkish^|77ab5adaea15d43f1a656d9d741d5efd1850e260^|4.16^|x64
echo Ukrainian^|306cfeae4d50ed36de4ca57113548086c1dba0ee^|4.16^|x64
echo Arabic^|fd86b4ce64567cf0f2631543be131a2a332ae3b0^|3.08^|x86
echo BrazilianPortuguese^|f1550e9c36b45180bab85664d6dddd635a6eb8a4^|3.08^|x86
echo Bulgarian^|046b056d17eb0264f1683b465f086e2761a987ef^|3.15^|x86
echo Chinese^(Simplified^)^|b21c1ef5445f0c0a185ab15eaf0e953b737ca55c^|3.32^|x86
echo Chinese^(Traditional^)^|c49acd10076fcc91a413176a3ea65dcb2ca3beb0^|3.29^|x86
echo Croatian^|cfb771bffe40baeafba6c7f805251d07c3a780ad^|3.12^|x86
echo Czech^|be0064206618dbbb9136b9e6a4700da9251399bd^|3.14^|x86
echo Danish^|84505f4b7be2da8db23199e6fd27d63aac8b00fe^|3.16^|x86
echo Dutch^|8752addfe7b6fafbb98ccec1104f79a59902357c^|3.14^|x86
echo EnglishInternational^|c347151b94f33b6148545f3e51244dd3c54b1b0a^|3.25^|x86
echo English^|bc4b6cab40e6d60ca4fa8969b16374a2e9b724d5^|3.25^|x86
echo Estonian^|513e2154b9d042bcba3992244c02198f3c2f595e^|3.11^|x86
echo Finnish^|d2ed0364423727934a5fdfb217ea4466e6861e87^|3.14^|x86
echo FrenchCanadian^|1ddc54a242b4b6ef2463a9a6352c19c7bd4814a5	3.08^|x86^|
echo French^|a88431213a86d9fc8c955771b616ac1a63022c17^|3.27^|x86
echo German^|78822dbae0af88327f3b0765a6a7d23f4d11119f^|3.28^|x86
echo Greek^|958bbce223832196b48929dd0410dcdb2d0a4312^|3.17^|x86
echo Hebrew^|34f2f583450a6a3e695622c0d34d529713f68317^|3.05^|x86
echo Hungarian^|8e0805bbf765443049a925c1095b667a04614873^|3.14^|x86
echo Italian^|6cbc57f07c086f6de30c1891c9e8afe7f9ddc371^|3.19^|x86
echo Japanese^|0c7d0b6098d6dc40d811ac95d6da75e039365d33^|3.27^|x86
echo Korean^|21e77cca11d1266ba63d6fdbf51499b670e1513b^|3.08^|x86
echo Latvian^|2c52c808a43fb6935018fe538705815a2f3eaaa9^|3.11^|x86
echo Lithuanian^|77e50ccc7843bdf547cd9a996b4f51d4b061906c^|3.12^|x86
echo Norwegian^|510c3a3034bb92e07423869f630b487a573aa1ee^|3.14^|x86
echo Polish^|461efeef208bf6d53080130b9cd747fc9b706652^|3.16^|x86
echo Portuguese^|6eae9eae2c6807f625da78818b41af06f1aeaf3e^|3.18^|x86
echo Romanian^|ee939aa5eab93fc94a666db1d6e97541e002b84e^|3.12^|x86
echo Russian^|64823da9dd35f93d3b5c7f2681c966da7cb57c5a^|3.08^|x86
echo SerbianLatin^|c657b2f155da362db04f79408c8e7cc8f46600c4^|3^|x86
echo Slovak^|78c112f5ae1a25fc8e630976d67e696487b80f2b^|3.13^|x86
echo Slovenian^|8e59d17d9f44872dc144a7ef97d2e4c603eb021e^|3.12^|x86
echo Spanish^(Mexico^)^|d6a940980b5b3760e4521078d70bbbb641bf45da^|3.07^|x86
echo Spanish^|2de64dadf9185362f35afc61391a389831a09e64^|3.26^|x86
echo Swedish^|1fe00131e68f5c4dacde972343f56d07890b6650^|3.14^|x86
echo Thai^|b3313875a453de270e31c76d868029d4cacf53f5^|3.03^|x86
echo Turkish^|b33a3b0478f131e364458bec94cefd8bd487ae84^|3.02^|x86
echo Ukrainian^|84ff855355334e63f11a865f24030a9de9f788d6^|3.02^|x86
)
goto:eof
:================================================================================================================
:================================================================================================================
:================================================================================================================
:================================================================================================================
:================================================================================================================
::DBTB18092 STREAMLINED
::===============================================================================================================
::===============================================================================================================
:DBTB18092
>>database.tb.18092.smrt (
echo Arabic^|de626359145eeb49a4149d407191716c7fcb2715^|4.48^|x64
echo BrazilianPortuguese^|a3c21b871664db66c1a8d9eba71b28332ae4e94f^|4.48^|x64
echo Bulgarian^|ce154294d64c7d0c6f5ebcfc710f6c611ca53989^|4.61^|x64
echo Chinese^(Simplified^)^|81766e9fe5793e8781c5336e513ebceedd2f7b90^|4.74^|x64
echo Chinese^(Traditional^)^|b0f40399f54d739b7b22f5b45c9bdae2502d4bf4^|4.7^|x64
echo Croatian^|feb14a5656a24285567ac320516833e5e678af91^|4.59^|x64
echo Czech^|94d19203177653139e56d64a6576cf7667abf6df^|4.61^|x64
echo Danish^|f2a7c8c03d6fbc86ceab37a701fbf79b00c7486d^|4.63^|x64
echo Dutch^|442aa55ee3ec7c2a5837e7eb183a956846492fcc^|4.61^|x64
echo EnglishInternational^|1b7a6d1372140950b9fbb32900e2246c8682750c^|4.73^|x64
echo English^|bee211937f3ed11606590b541b2f5b97237ac09d^|4.73^|x64
echo Estonian^|c381dcbe9b7ff91972edcb89867b6c6b42c28d0d^|4.58^|x64
echo Finnish^|148269dcf8467a0b965b9822cf944fd5832f130f^|4.61^|x64
echo FrenchCanadian^|678d7540836397a454321a7902084c944042e6e7^|4.47^|x64
echo French^|9c0cc504ccf1412a124c46863cc2d46c36327666^|4.75^|x64
echo German^|dd55d5a14ea5a9870508e214d600e2bf44c72093^|4.75^|x64
echo Greek^|6ce1bfb389535d6fb2aa4e2e028d984e079dff05^|4.64^|x64
echo Hebrew^|4ea26a6b076c93ff7f2d29034ce2cf0a7cbb9cbc^|4.44^|x64
echo Hungarian^|b4f88e157d033941569644782a166cc9a73f35b8^|4.61^|x64
echo Italian^|b66dbfee26d68682b653cdbba2af8da320e9802d^|4.66^|x64
echo Japanese^|f1cc02f2a6588a32f691d6f07da6726b1a340b9f^|4.67^|x64
echo Korean^|26ef0a6a7a40895385852f4d3b1326235a966342^|4.49^|x64
echo Latvian^|411bc9049baff178eb1badfb995ff12f0d465ca0^|4.58^|x64
echo Lithuanian^|4222684c6aac58dc735a014f4e8246c1c8b412c5^|4.58^|x64
echo Norwegian^|ec840bb6ef97871f86ffb162b1cbfe0d71deee9d^|4.61^|x64
echo Polish^|ad1b204aeb5d2145bcb326f6a77be78a61ba4a3d^|4.64^|x64
echo Portuguese^|4cfc2d2b52e3d2d2d65a580bfdbd6caba2acf668^|4.66^|x64
echo Romanian^|5341cb961001d1f4b5311ec3aad1107f4a2f440f^|4.59^|x64
echo Russian^|32ba5564b862fb479b7d5e31f6e1604107dfe07f^|4.49^|x64
echo SerbianLatin^|97dd94df424b472009a2f85ea80603ad216cbe4c^|4.4^|x64
echo Slovak^|804325803bb454a7bbf0b61b12c8a5e53e1953b3^|4.59^|x64
echo Slovenian^|a06574263779581eb9fa9861c1913e90eb30cb62^|4.59^|x64
echo Spanish^(Mexico^)^|9baec60a872abb8b2fd458b9e1dd0f561bd146a3^|4.47^|x64
echo Spanish^|420270676e73ded2c6fb777edebb1a46eb9357e7^|4.75^|x64
echo Swedish^|5c1122e4c90e828e78386bee6e0eec1b7c9f5551^|4.62^|x64
echo Thai^|b5a235b46bb6602dd097a50f71d97eae6dc2d6ea^|4.43^|x64
echo Turkish^|5c094a717bcda54a2401b1b1c39ac04947621e38^|4.42^|x64
echo Ukrainian^|0ab01954daed506f9ad2b337a4b20a5a98e3b464^|4.42^|x64
echo Arabic^|2c4e8f608517d58e53e7968949c05dbaf891b097^|3.21^|x86
echo BrazilianPortuguese^|26e94eae1c23ddecec68d11c135be5782501e70e^|3.21^|x86
echo Bulgarian^|0e1c11d537227b1061e44a64ac5df58e7994e954^|3.28^|x86
echo Chinese^(Simplified^)^|6e060995a1529b6a664bfdecefb72985601b9897^|3.45^|x86
echo Chinese^(Traditional^)^|dc97606dbb0a1808e9172f649457f6dc0a287cdd^|3.43^|x86
echo Croatian^|9ef24cfa800e791cdd5867be4678d69eb9a3f1e3^|3.25^|x86
echo Czech^|05ea75a2b71c2cb851f089740d75df77e5479391^|3.27^|x86
echo Danish^|0996a24c91fd76c386f90a323863f244d022bf48^|3.29^|x86
echo Dutch^|7a2a1b69e336889f3aa2517b7ac2417f8a2818ce^|3.27^|x86
echo EnglishInternational^|265c8c0d86ce55173c29f727b7634e191d5e751e^|3.38^|x86
echo English^|cae018b8e164fcbd06cd680e12ec4d264d736291^|3.38^|x86
echo Estonian^|cfd4dbda96e795396732f1ec9103a14951cded4f^|3.24^|x86
echo Finnish^|b6279bcbf317cb97fa39f55c69cd94735fd60c50^|3.27^|x86
echo FrenchCanadian^|256ca03b8f1f52e8b043d94b50e03262e1526f9f^|3.21^|x86
echo French^|d363480055eb103a8347393849659f39ef930bcd^|3.4^|x86
echo German^|49bf9963cc55dd137ffb554c95f6f3fa75c654a8^|3.41^|x86
echo Greek^|0f09b983a16e5cd2879be9e02f156a61d99a4e46^|3.3^|x86
echo Hebrew^|35b0fb6199a8dfa28b21f3ee19b1bd97f92db33b^|3.18^|x86
echo Hungarian^|568e53535ff1712a5aed28fa71b3d44ca15e1a58^|3.27^|x86
echo Italian^|5e10416c481bb2a9770c9bd4133822da021a167c^|3.32^|x86
echo Japanese^|843586cac656ebb3c48e8288cc4321b10460aeaf^|3.4^|x86
echo Korean^|a2dd8415d7ffa78b4bca1a666d73d2ca1adb3879^|3.22^|x86
echo Latvian^|83bdad2ae39341d2bfe181531759ed396b73836d^|3.24^|x86
echo Lithuanian^|92edbea2ff81604ea3629ad96ef9ed2c8d262338^|3.24^|x86
echo Norwegian^|ebeff6c4630c6b62dfdb9daed72ab9eaf30caeab^|3.27^|x86
echo Polish^|9a9f89cb36aeffcef68169009e70837eeaed8154^|3.3^|x86
echo Portuguese^|d849f076c80282fdb2ff20b0daa3436ee7b54720^|3.31^|x86
echo Romanian^|407f5a2d9fb2eee6235daf63548adc384ca72564^|3.25^|x86
echo Russian^|61e800282576f1dc5ccef8af5fa92151a761ece1^|3.22^|x86
echo SerbianLatin^|ad152ecfd0a996e0df095ba70489bd3d9c689b5b^|3.13^|x86
echo Slovak^|1fb961565c55e15fd18bf9016e4b55d3f987cf1a^|3.26^|x86
echo Slovenian^|96d0df9d6276b2b101e9819bc0dde46b2a9be6bd^|3.25^|x86
echo Spanish^(Mexico^)^|cda4ea8710aa85afc54cc01b7db90a6eb109b378^|3.21^|x86
echo Spanish^|354337e22469a657bccf7eb67fb355d03006d15d^|3.4^|x86
echo Swedish^|6da40856077e2f11f441eeb06061c0d6e6ce1572^|3.28^|x86
echo Thai^|bc1b90d2e3f711272e3d1a7a3c8c1ba93dde774f^|3.16^|x86
echo Turkish^|24233b9f8a9a03e2662efc7e0104ab07941568aa^|3.16^|x86
echo Ukrainian^|4930123fc3b9dc48b01d9e4d78f14d8354b59286^|3.16^|x86
)
goto:eof
:================================================================================================================
:================================================================================================================
:================================================================================================================
:================================================================================================================
:================================================================================================================
::DBTB18093 STREAMLINED
::===============================================================================================================
::===============================================================================================================
:DBTB18093
>>database.tb.18093.smrt (
echo Arabic^|0dfee832d42c7cf29e8b85531a178038ef32618d^|4.75^|x64
echo BrazilianPortuguese^|470009a495a3a0870de9ac044966d22014d2bcec^|4.75^|x64
echo Bulgarianc41ae9764e7df8527a2a31be0306375100aad93f^|4.9^|x64
echo Chinese^(Simplified^)^|62afbebafb43db7ca5f37d426c16e393060fc15d^|5.01^|x64
echo Chinese^(Traditional^)^|dea12b513f4463c899050d27b442745229bf4d7a^|4.97^|x64
echo Croatian^|d9c4b00845bccc51221f20aacec6166e621f157c^|4.88^|x64
echo Czech^|e485b6578b580b07252e6219afb7c27f5c13b142^|4.9^|x64
echo Danish^|4dd1b48a72229a7c7bb3cc6d9bd96edc0b8daba3^|4.92^|x64
echo Dutch^|7d1fe5550890781095537f1a4cc7ddc6851e190b^|4.9^|x64
echo EnglishInternational^|d9db5b129803f6baf3da1e1c31920f3c78b2c3ce^|5.02^|x64
echo English^|e448fd3ceb724a924a2afce17d80739329c30c74^|5.02^|x64
echo Estonian^|cd52a063796dd11fa7026ca3e8adebbeaddf3b31^|4.86^|x64
echo Finnish^|9b8c63fc419af14bcaa23ebffff85b29a323415f^|4.9^|x64
echo FrenchCanadian^|ba523c996a0568c72a227effbe8b96582bc93138^|4.74^|x64
echo French^|9729a28dde2d02b6054da8f4147e406ebd76872f^|5.04^|x64
echo German^|a537bee75dc264da904640238d18ab424c980f71^|5.04^|x64
echo Greek^|3cbdd06a9533467d14032d7ae56adb02550b47d1^|4.93^|x64
echo Hebrew^|d10c443c6b90f4e61debc4b25ae81305bbb2eda3^|4.7^|x64
echo Hungarian^|7f73bff86fbb4058b4aae1b7fe69505bb3828a9a^|4.9^|x64
echo Italian^|54c4d32a2b39f6c57517b6d6589bbae9bcf01aca^|4.95^|x64
echo Japanese^|bfb1190b85b86372a3bbcb486836a9665cfffd9e^|4.94^|x64
echo Korean^|a7c88c1e7cb464fc6269860c1a4f0be00d4dade2^|4.76^|x64
echo Latvian^|ef9220605d8e3ffea88bf764ee1676c499d58782^|4.87^|x64
echo Lithuanian^|3a127d3d540d4a5db2e9d8552c240794374c9882^|4.87^|x64
echo Norwegian^|1b37af0a87ddeff8bcc68a1f920dcfab01c44dad^|4.9^|x64
echo Polish^|d1807dc7e64eaa9bda06811ebc80c796831930db^|4.92^|x64
echo Portuguese^|76825e86e16b27430a8693a1ef9dea41607a0546^|4.95^|x64
echo Romanian^|8daca6b72e381f81603d6a4e60be93e7efba9932^|4.87^|x64
echo Russian^|d00ddc1750b796111c6caa7b342bd6d2cf04ddae^|4.76^|x64
echo SerbianLatin^|9ca548e1d983409825f72787212308612d80a658^|4.67^|x64
echo Slovak^|14d6abb92d655c3102b6801b659bd4c772161b12^|4.88^|x64
echo Slovenian^|0e4beaf951ed907bf1d0a4e897149794a67e86a4^|4.88^|x64
echo Spanish^(Mexico^)^|839243e470baeb364035d8ccfa2bd8153cfda1c3^|4.75^|x64
echo Spanish^|bba5ae7e6ce43a2b13e35d380fe4c06ee83c85a3^|5.04^|x64
echo Swedish^|17d5428c7d1b81a0f0301cb7ed0a9dc0529a2b74^|4.9^|x64
echo Thai^|d1143ad9c008cf46ec7e2611cf838299dfd2688e^|4.7^|x64
echo Turkish^|a3a84c7329963a578520c5a81967fae2e0270d9c^|4.69^|x64
echo Ukrainian^|8933b4af14b0a6ab802c6e1fabe9144dd45d39f8^|4.69^|x64
echo Arabic^|4744cbf3889c8ffda2bbe7fa69f8db979e14de03^|3.37^|x86
echo BrazilianPortuguese^|ab25229d75f0961a5ec62ed1a12ab670a3bd93ed^|3.37^|x86
echo Bulgarian^|e71429b131cdda4d14f79af7a8e5e41a87d3c738^|3.44^|x86
echo Chinese^(Simplified^)^|2d6ee73aae19d1103f9887fc8ac83592f36d6c67^|3.61^|x86
echo Chinese^(Traditional^)^|14465be64c7e2b940ee64e8d65bd46401ad309fe^|3.58^|x86
echo Croatian^|b4df068adf3747f0751c0d4dc3b62841a0728355^|3.42^|x86
echo Czech^|f23a9242e36f2fc074778bf67da4f5850779e29d^|3.44^|x86
echo Danish^|0438eaa9a209226ec54ab7e489ab9c691e17ace8^|3.46^|x86
echo Dutch^|858ac6fc21c6c01285e80e58d2295b54509d6881^|3.44^|x86
echo EnglishInternational^|8b863744a317da818185763ae492e6301e3a8496^|3.55^|x86
echo English^|0290d3d4530a0bcb82d03ea8d6237a32e32416df^|3.55^|x86
echo Estonian^|965357a1d47359705660d0ef02b9a1a743d1ece5^|3.41^|x86
echo Finnish^|07cdcaa202314b29868f10fcd464c559409cd026^|3.44^|x86
echo FrenchCanadian^|fd0f564aea87deedfaf4b9bc441dcafbfee46744	3.37^|x86
echo French^|327f4219ba11b7b06fc13e27f035bff01b9061b3^|3.57^|x86
echo German^|2bb85123dc22f00e2bd80f1f2ff9ea3578b5e69f^|3.57^|x86
echo Greek^|d8c409ebc6704f747d759a5e91b406a975738a6d^|3.47^|x86
echo Hebrew^|1ec9858259a01566c1e7b84a5d8372d84cdc75f5^|3.33^|x86
echo Hungarian^|4c7c40f3ea59c01c7420cb67ec4e440b60115456^|3.44^|x86
echo Italian^|40147ec9e95b855678cc33bc8ddc2b9b383d6ab8^|3.49^|x86
echo Japanese^|b1ae3c716313570bb75851c4797a381d0bb5887f^|3.56^|x86
echo Korean^|24a04e73659329dde1c875606f690246c1703ac7^|3.37^|x86
echo Latvian^|4653b296d7ae13249d99f5c55dbc29a93dc87a11^|3.41^|x86
echo Lithuanian^|a967fc07a54fbf64510a94e8bc8ccf3f95dec63a^|3.41^|x86
echo Norwegian^|0931e5a766572ef43ed75b354125402e82db6fe9^|3.44^|x86
echo Polish^|f99cb4849ef8b7d250fbee45c1309c548575c3f8^|3.46^|x86
echo Portuguese^|bd221aaaed63c03c05b16c2658c792a18921dc6b^|3.48^|x86
echo Romanian^|03f4e027e523a901fdc8535d64ad975e012d9c52^|3.42^|x86
echo Russian^|81613d9a290ce89c5f5b25e4a3408e69c2c1f882^|3.37^|x86
echo SerbianLatin^|6efee82975c09e9d9a0d10259acdf6fa05c1be90^|3.29^|x86
echo Slovak^|113783f80a0dbc219ef531d6e7788e257a49abae^|3.42^|x86
echo Slovenian^|b0dee05be6096e9c9cf491b79190d218fd03cd95^|3.42^|x86
echo Spanish^(Mexico^)^|b9b9fb8d50b9edc5e5b1c8e15e94bed9fd0a602e^|3.36^|x86
echo Spanish^|dc1b55355ed7a21117421c985af8a324723e58e5^|3.57^|x86
echo Swedish^|d472b6c9f7c9e941b6e55ecdc02c185092fb9e67^|3.44^|x86
echo Thai^|0eeb6894da839d29ce4fc1e481b977d55253a0d1^|3.32^|x86
echo Turkish^|f3ce06e2bb6ab090acff78c270194af2df84ea15^|3.31^|x86
echo Ukrainian^|45b6d92d867e8aeb2b8833dfed7430044b8ca224^|3.31^|x86
)
goto:eof
:================================================================================================================
:================================================================================================================
:================================================================================================================
:================================================================================================================
:================================================================================================================
::DBTB81 STREAMLINED
::===============================================================================================================
::===============================================================================================================
:DBTB81
>>database.tb.81.smrt (
echo Win8.1_Arabic_x64.iso^|48aefc696634b14482443e5e92ca3935c64f3f03^|4.05
echo Win8.1_BrazilianPortuguese_x64.iso^|619bfd703f58716a90434a92531039ec44f83baa^|4.02
echo Win8.1_Bulgarian_x64.iso^|bdf8307802115beeae6106e3bc70e919013284b2^|3.97
echo Win8.1_Chinese^(Simplified^)_x64.iso^|9a85eac2d20446e3e086bf57a7dfc8832fb3ba56^|4.2
echo Win8.1_Chinese^(TraditionalHongKong^)_x64.iso^|21bb15886d8d9b7c1cde63d34ab7be5cf17b1e99^|4.18
echo Win8.1_Chinese^(Traditional^)_x64.iso^|dfd583a9198b8e06bd9e7c1ea6177ab2a1973c9a^|4.17
echo Win8.1_Croatian_x64.iso^|3b5c0d0cbd956903801b9319384f4a78988eb8af^|3.97
echo Win8.1_Czech_x64.iso^|0e572b62ea98a6e007e02742c7d087811bfae3a9^|4
echo Win8.1_Danish_x64.iso^|dc7948ef523f16e770d34ce2c0ab22168bbc8907^|4
echo Win8.1_Dutch_x64.iso^|00b1b0c90feb1012adb90eafbf9c3d8d3f04612a^|4
echo Win8.1_EnglishInternational_x64.iso^|c99a6ec2de4a71def522989d2242cb34ab860a0d^|4.03
echo Win8.1_English_x64.iso^|a8b5df0b0816280ae18017bc4b119c77b6c6eb79^|4.02
echo Win8.1_Estonian_x64.iso^|03766dfd1240e40741476ad0db3816590e68e806^|3.96
echo Win8.1_Finnish_x64.iso^|627a9e8c84cc180ed3a84f5d9475e853210afadb^|4
echo Win8.1_French_x64.iso^|7b9f1f0870d7e4125be45b64b40182070417f7f6^|4.06
echo Win8.1_German_x64.iso^|07c464680da474a4d6c23d57d99a90f129f85584^|4.07
echo Win8.1_Greek_x64.iso^|077a3faf9a110035d05b4246e678ecb0e7d8a7b5^|4.01
echo Win8.1_Hebrew_x64.iso^|ca59f6c3ad4a1225ea3b8eb17ec55a42c5a29a39^|4.01
echo Win8.1_Hungarian_x64.iso^|da541398069d067e3e388b36cfeea898f55091d4^|3.99
echo Win8.1_Italian_x64.iso^|a5292abf4784f162399ea4f7402c4674b1182dea^|4.01
echo Win8.1_Japanese_x64.iso^|3b35ed1690accfd0078f03a24b8c44e3cffac6b5^|4.09
echo Win8.1_K_Korean_x64.iso^|ebdda17a65d8d3f4ab6a18bed2642eac21bda15d^|4.06
echo Win8.1_Latvian_x64.iso^|97e8bcd8b80accd276a9602aea7d51538ef5e37f^|3.96
echo Win8.1_Lithuanian_x64.iso^|fe8ec566e513b5d5e56bc7c07f6f936f1e324be0^|3.96
echo Win8.1_Norwegian_x64.iso^|f6181ffd851d55f29b6bf99503ac2c1b2757d012^|3.99
echo Win8.1_Polish_x64.iso^|dcd1ea4675c41c10452910f4cf9dfa1905dd26f4^|4
echo Win8.1_Portuguese_x64.iso^|c3fa9bad9bba2eaff519df6c06b3cba482cd9c6b^|4.01
echo Win8.1_Romanian_x64.iso^|14c6223f0bf8a8ffa086ce52faf6fc3206b19362^|3.96
echo Win8.1_Russian_x64.iso^|8d69de249806ffbe94870adc4d74e40e486c13b6^|4.02
echo Win8.1_Serbian Latin_x64.iso^|9ea5300de7ea822dcbbf52a3103a25e988c492f7^|3.97
echo Win8.1_Slovak_x64.iso^|ad26428f7900775c792135616ed04053b44c2e3a^|3.96
echo Win8.1_Slovenian_x64.iso^|c51922f4d9bd68a298762346ed066ebb2f2f3c6f^|3.97
echo Win8.1_Spanish_x64.iso^|b2c02e02c70569eb0cc7a912f72b1c3417c67e01^|4.06
echo Win8.1_Swedish_x64.iso^|40f2bc8a3c44fb5a7adf979493f789bd49ad90d5^|4
echo Win8.1_Thai_x64.iso^|ca4c2475d8817f15705e8343b3a11993be4fb8cf^|3.97
echo Win8.1_Turkish_x64.iso^|776e4a514c020947e402eaeedabb7431440d2099^|3.99
echo Win8.1_Ukrainian_x64.iso^|271ce4be9731e24ce541c2769063052fe75fe574^|3.97
echo Win8.1_Arabic_x32.iso^|c9d5a6a85bc01ef59821600f3e62b982fe3d70cf^|3.01
echo Win8.1_BrazilianPortuguese_x32.iso^|309d4cd54c658ac079905a92d1113f96f23083da^|2.97
echo Win8.1_Bulgarian_x32.iso^|37d37118d49ae56f4964c1aef91bd1fafe94ee56^|2.93
echo Win8.1_Chinese^(Simplified^)_x32.iso^|abbf814846c425c88910b011bc9d3d3c77932056^|3.12
echo Win8.1_Chinese^(TraditionalHongKong^)_x32.iso^|d076437f650951a61405bbc99ec3b99bad1406ed^|3.12
echo Win8.1_Chinese^(Traditional^)_x32.iso^|0c371428ab14579a609a42a7407c7a176cd09bf3^|3.11
echo Win8.1_Croatian_x32.iso^|3f62e5ed076f6c3aa37775865850fc474462bf54^|2.92
echo Win8.1_Czech_x32.iso^|c394e8869a7f4ff5aebbe3bb473052b91d922dec^|2.95
echo Win8.1_Danish_x32.iso^|d7c98abcc4818d7fa34ed3809e960048b8b11bd3^|2.95
echo Win8.1_Dutch_x32.iso^|348a0d8e124206e1120eced85075ff870b592092^|2.96
echo Win8.1_EnglishInternational_x32.iso^|39c57b6d35eaaab64a971c374fb35004709e1f2c^|2.99
echo Win8.1_English_x32.iso^|c7fa828e01e98b601e0aca8019f1cb223eb23223^|2.99
echo Win8.1_Estonian_x32.iso^|7eac744280611049026d259b0185b8664945c602^|2.92
echo Win8.1_Finnish_x32.iso^|f93e895c5f95775ebddb4f975c386ef7441e09c3^|2.95
echo Win8.1_French_x32.iso^|1bb67f65d68ace78f7853f9ccbc506333eacceb5^|3.01
echo Win8.1_German_x32.iso^|18ac1bf6e2e0d00f58071c0acd92334876202282^|3.02
echo Win8.1_Greek_x32.iso^|56dffeebc3baa4140266b009c5838963e4f4a6be^|2.96
echo Win8.1_Hebrew_x32.iso^|24a238906f458f75bb2e7ff17f6076552265505c^|2.96
echo Win8.1_Hungarian_x32.iso^|cd29b4a5a2c1d858eea3455be60e15f7e23c0fc3^|2.95
echo Win8.1_Italian_x32.iso^|ed3c00086de57049ea65f72e87cba247be7c0399^|2.96
echo Win8.1_Japanese_x32.iso^|adb04f95bf292151f4bb1c8bb989812c2c2e76b8^|3.03
echo Win8.1_K_Korean_x32.iso^|bc42b12fedfc72f9eac48b235c7ae8700f877fd7^|3
echo Win8.1_Latvian_x32.iso^|389f531a6b0c3858fecc862fa2558f0fa0a00b5e^|2.93
echo Win8.1_Lithuanian_x32.iso^|02960cc1cd23b566639571e5bf3e5f247588b8d5^|2.92
echo Win8.1_Norwegian_x32.iso^|3490ca5e3f4ff6efef17998917a402abd2455a09^|2.95
echo Win8.1_Polish_x32.iso^|62d35270f71e804ffb9fed7ecfbbe201f29c2472^|2.96
echo Win8.1_Portuguese_x32.iso^|f4a6d35e19dd2042274681fff9e6198811bd49cf^|2.96
echo Win8.1_Romanian_x32.iso^|9c7a99dad2372c3f338c1d617e9a23ebee6b4194^|2.94
echo Win8.1_Russian_x32.iso^|fb10a76d2c2f8db76add14f83c41b10b3994ffb2^|2.97
echo Win8.1_Serbian Latin_x32.iso^|b574e5da71625b4ecaa3849a9d441328d83f7454^|2.93
echo Win8.1_Slovak_x32.iso^|c0e62a806d17086e6ecbc71fd901bc253fc383f4^|2.93
echo Win8.1_Slovenian_x32.iso^|56358b0ff05752b98f9150148ff3b254c2bb3808^|2.92
echo Win8.1_Spanish_x32.iso^|8c2e8287dcb05c387a28fef1d08e59fd7907c8cb^|3.02
echo Win8.1_Swedish_x32.iso^|de89ee967586722df3c445db13c5a109c4244336^|2.95
echo Win8.1_Thai_x32.iso^|db2eedcaa8fef4678caf0fa3175682cfa34f750f^|2.93
echo Win8.1_Turkish_x32.iso^|6b41ee4936c062b3265514cad16e37999ed3bf8f^|2.94
echo Win8.1_Ukrainian_x32.iso^|f6582a901ba7ac46b13cee54195118faeca8cf28^|2.93
echo Win8.1_Pro_N_Bulgarian_x64.iso^|a675314c96abdefd82aac8327d0274ae02638e79^|3.74
echo Win8.1_Pro_N_Croatian_x64.iso^|690deffc247bc364cb65bfe436e04ceacae13ee6^|3.74
echo Win8.1_Pro_N_Czech_x64.iso^|b852b9e84d4f70c59eac6b7a1eacca9337f28e3e^|3.77
echo Win8.1_Pro_N_Danish_x64.iso^|b66260a8e6d0b9a96f26b9f5b0944c589caf62c7^|3.77
echo Win8.1_Pro_N_Dutch_x64.iso^|c8e00db23b8e32b26803b537351a6f5fc871c705	3.78^|
echo Win8.1_Pro_N_EnglishInternational_x64.iso^|df2365bdd9e60baf16ed30492345e14bbc03509e^|3.81
echo Win8.1_Pro_N_English_x64.iso^|457d02bd26a19965da172daae78f985cf53af692^|3.8
echo Win8.1_Pro_N_Estonian_x64.iso^|cbe42a19fbe08bba8ea2742bd608f7e3ddb7e614^|3.73
echo Win8.1_Pro_N_Finnish_x64.iso^|aab2f6bd18132bd7d543d85951a18fd82c6a9c68^|3.77
echo Win8.1_Pro_N_French_x64.iso^|7bb1ba822a9b88e617fc78a5db461edda6664029^|3.83
echo Win8.1_Pro_N_German_x64.iso^|833ab9b7d66f0d602e0553b23111401170099df7^|3.84
echo Win8.1_Pro_N_Greek_x64.iso^|6dfc65f3960122a2b3690988d1a22471676e4804^|3.78
echo Win8.1_Pro_N_Hungarian_x64.iso^|e6548d1d14a5621389943d52a889d5f521dd90e3^|3.76
echo Win8.1_Pro_N_Italian_x64.iso^|182064d7947b1bb703387a4d2af81ab594ab6cd0^|3.78
echo Win8.1_Pro_KN_Korean_x64.iso^|37f78e32fe8cb23b4ff1f5b49bcfbf07d278aed5^|3.83
echo Win8.1_Pro_N_Latvian_x64.iso^|c8b7d5040a2eeaf9982470527874b32ca1f97cc5^|3.73
echo Win8.1_Pro_N_Lithuanian_x64.iso^|d49708f08f45ca2afe6045e2a60cecd45714fcb5^|3.75
echo Win8.1_Pro_N_Norwegian_x64.iso^|1f06421d9263eb3c7b7fc6e0cbf10e662ff617bc^|3.77
echo Win8.1_Pro_N_Polish_x64.iso^|ecde38990d7c635e7b768284763a37f73a31d526^|3.78
echo Win8.1_Pro_N_Portuguese_x64.iso^|972486c70496793f336539e334ce8f215ce739c6^|3.78
echo Win8.1_Pro_N_Romanian_x64.iso^|b0fa6038cad66a1844d735025ed3a96c08050e43^|3.74
echo Win8.1_Pro_N_Slovak_x64.iso^|40f9c1ad24aa98e85f2bea67d32eb3d8851f666d^|3.73
echo Win8.1_Pro_N_Slovenian_x64.iso^|96d909ace95119b4f11f2c36274d0cdca8ce81df^|3.74
echo Win8.1_Pro_N_Spanish_x64.iso^|d82fbb9edc481bff8b54068b63949cf28b8080a8^|3.83
echo Win8.1_Pro_N_Swedish_x64.iso^|381c5bff4804fcc2bda10ebc7d071592ead1c7ea^|3.77
echo Win8.1_Pro_N_Bulgarian_x32.iso^|9e8755f1db5bc8a0d04d99d1ebf8c6ff2954073f^|2.76
echo Win8.1_Pro_N_Croatian_x32.iso^|403a243ba8269667c5c797a9864a34cd481fdc13^|2.75
echo Win8.1_Pro_N_Czech_x32.iso^|2c72490d3e3a2a69d07eb5c60a40b88f2f71ec82^|2.77
echo Win8.1_Pro_N_Danish_x32.iso^|e6b5487166c34cff5bd67d50fcf338d2ca7b0f32^|2.78
echo Win8.1_Pro_N_Dutch_x32.iso^|b26b6f327583a754f72ce158e707d6b030bf9fcb^|2.78
echo Win8.1_Pro_N_EnglishInternational_x32.iso^|7b96238d9c30aaadc984ce226f98cf8a3bcd1fd9^|2.82
echo Win8.1_Pro_N_English_x32.iso^|a2c2d9916a4aee87afdd0aea19459f2f8414737c^|2.82
echo Win8.1_Pro_N_Estonian_x32.iso^|140888f5c2a6bdfc4b7fa8755c8e0b91f3f9b76f^|2.75
echo Win8.1_Pro_N_Finnish_x32.iso^|d801ac4b31a9840365108e16c22355c8dca7d6af^|2.78
echo Win8.1_Pro_N_French_x32.iso^|d8910bd0f8645ffa91dcb4bbea64117f018a9256^|2.83
echo Win8.1_Pro_N_German_x32.iso^|2592b1d176f6392531dcef7a97e3ec4a6f6ac660^|2.85
echo Win8.1_Pro_N_Greek_x32.iso^|45c83668f0fff287d8adde84a5daad5f9855f436^|2.79
echo Win8.1_Pro_N_Hungarian_x32.iso^|9a01670e80c8d0c6f0151073976c53cbb55ff885^|2.77
echo Win8.1_Pro_N_Italian_x32.iso^|8ed2af183ceb1073674734ad7dcf0c1ca9cb313e^|2.79
echo Win8.1_Pro_KN_Korean_x32.iso^|c120e8b4fc39641867d0f6ce99139f0173c7e4f9^|2.82
echo Win8.1_Pro_N_Latvian_x32.iso^|eaf2cb6b1c5064a2834cbfd19b72d4d3f69276b9^|2.75
echo Win8.1_Pro_N_Lithuanian_x32.iso^|4f03cbafff47aac1a7c0bd3e42f487916316a70e^|2.75
echo Win8.1_Pro_N_Norwegian_x32.iso^|b3a4cd3b6447581fb098ea20240655d01d576432^|2.77
echo Win8.1_Pro_N_Polish_x32.iso^|9d87d88103f39cd42c48aecc623dd6bb769693ef^|2.79
echo Win8.1_Pro_N_Portuguese_x32.iso^|d47199eb9b29a58e71828cb7279ea4f86a63f273^|2.79
echo Win8.1_Pro_N_Romanian_x32.iso^|44a0779f37030910f66f689c6fb144f36a43a79a^|2.75
echo Win8.1_Pro_N_Slovak_x32.iso^|ea9c8e158dbb2143dfdf2a03beec8353ce52d48c^|2.75
echo Win8.1_Pro_N_Slovenian_x32.iso^|517a9f3697aab09ebfedb0dfd0ff2a6f2fcc5384^|2.75
echo Win8.1_Pro_N_Spanish_x32.iso^|8ea5d452d000b59478437d69d4dc254ce6f8367a^|2.83
echo Win8.1_Pro_N_Swedish_x32.iso^|da132c0c919c70c3a220df36fbede774fb152fd4^|2.78
)
goto:eof
:================================================================================================================
:================================================================================================================
:================================================================================================================
:================================================================================================================
:================================================================================================================
::DB1903 STREAMLINED
::===============================================================================================================
::===============================================================================================================
:DB1903
>>database.1903.smrt (
echo ar-sa^|d4945d9cc07c5606002ddf6a979f90d624b78fa4^|ar_windows_10_consumer_editions_version_1903_x64_dvd_3df6449c^|https://x0.at/oE9^|c398fce3efaf41133d12e4f177ef41bc557c64f8
echo bg-bg^|35ec8d1da7fec8b49f23459be9237fed45e241cb^|bg_windows_10_consumer_editions_version_1903_x64_dvd_a77f30f5^|https://x0.at/omk^|ca409688ed63410b1ff81f560cd2bbbc13e94d08
echo cs-cz^|b8e9e3e5086a05b9c76ccd80b548fa87c30e0ffc^|cs_windows_10_consumer_editions_version_1903_x64_dvd_45f449f5^|https://x0.at/K04^|07d71ce4f1f9b0f538f60f997baaa6a7378a6e32
echo da-dk^|4d49dc2999adef09054b175f675564c00ec5b108^|da_windows_10_consumer_editions_version_1903_x64_dvd_121925d9^|https://x0.at/Gqy^|5e46e22a34387ec16cd0ec42bf83b3849fd37c41
echo de-de^|38ba408a9cc1cb137b0242bd8b4ff1cc1dae9e6e^|de_windows_10_consumer_editions_version_1903_x64_dvd_02343d69^|https://x0.at/BoV^|171029d67f290cf07a61add7772cb836de3f1035
echo el-gr^|a36d9c24a6fbd7bf7e9b864752ba64e11ca7d421^|el_windows_10_consumer_editions_version_1903_x64_dvd_463b9d7c^|https://x0.at/t8m^|a5947c80d31a6f48afb4e5ff474f27048540811b
echo en-gb^|10972d3072ee715a6c8e2001754220c9147ef78b^|en-gb_windows_10_consumer_editions_version_1903_x64_dvd_44ca01a6^|https://x0.at/bjV^|97a3cae8e3c0c4647c729dbf529e2c76e7d04a18
echo en-us^|344ca92459c23663d5f857c72a7c030f85f19be8^|en_windows_10_consumer_editions_version_1903_x64_dvd_b980e68c^|https://x0.at/bjV^|97a3cae8e3c0c4647c729dbf529e2c76e7d04a18
echo es-es^|803ea7febc2030f3f01e3990c5ca84876f792686^|es_windows_10_consumer_editions_version_1903_x64_dvd_b315cd30^|https://x0.at/Pw-^|080d249f4742c46c437c8e930f2100f01cdd4505
echo es-mx^|f1610c919238b9795bca596248d69078ddc66587^|es-mx_windows_10_consumer_editions_version_1903_x64_dvd_ab27dbce^|https://x0.at/LIE^|5c734b243aef381292dda2293e6cadf0ea15bae3
echo et-ee^|4b47eaf826221bcdaca4e7da410ba3aae54518e7^|et_windows_10_consumer_editions_version_1903_x64_dvd_9baa9830^|https://x0.at/Hmy^|36f1cf7eb41a83236b2cb7014191c761415a9ed9
echo fi-fi^|78be2f09070088eeedcae9e1da5c4eb40cfc1ed7^|fi_windows_10_consumer_editions_version_1903_x64_dvd_1bbcf93a^|https://x0.at/cXk^|8b117f908cd9be67e2b5a5200a9ff2cd72ca313a
echo fr-ca^|e927fab0cb4a76c55ec70d79ea49fb093b4d9247^|fr-ca_windows_10_consumer_editions_version_1903_x64_dvd_88611589^|https://x0.at/e5o^|37f63d09e783062c4e38d6259886079bc345858f
echo fr-fr^|dfe4d1c27e2d40d52e7d4312b9b8b506b96fc909^|fr_windows_10_consumer_editions_version_1903_x64_dvd_04158601^|https://x0.at/fUD^|b3efbd5c1d4f26bf88e4b09647c76276ae41db17
echo he-il^|c2c710a256f9d9a90b393553362f88037a755213^|he_windows_10_consumer_editions_version_1903_x64_dvd_f69c5d4d^|https://x0.at/CFQ^|1b84138bbd6280d2354642edab839ba57f81ba69
echo hr-hr^|3212ab825838a7eddec42df1f089f5261384dcc6^|hr_windows_10_consumer_editions_version_1903_x64_dvd_0e10442f^|https://x0.at/FuW^|ccb2f93b8a147921d61eeb782ed778b143941944
echo hu-hu^|be3429dc8fcdbc2a1408bec2a49f9d7dcf356c12^|hu_windows_10_consumer_editions_version_1903_x64_dvd_012468a2^|https://x0.at/rFi^|1cfb191409b322bba64288934623489052ccc536
echo it-it^|b7fb1f0da08468779ab96d9516a1c0b9aa45c80d^|it_windows_10_consumer_editions_version_1903_x64_dvd_218809d4^|https://x0.at/U9H^|1d204d7415f0e389b5ed40603f8feb1e7c7067e5
echo ja-jp^|7c278b800bf511009d690287ed03f59d2f6bb14e^|ja_windows_10_consumer_editions_version_1903_x64_dvd_2d96ee2c^|https://x0.at/qmf^|3dd68ab327d1f5f8f9d92e52c21e14af69a37e4b
echo ko-kr^|b9af3ae8dfb2a567e59a1259970f1f72a57cef85^|ko_windows_10_consumer_editions_version_1903_x64_dvd_64be459e^|https://x0.at/5_q^|770ff186b342207eb3e979d90a4fd00180f50f1a
echo lt-lt^|c84553b2795cb51bf1aef7c41fd78695c8205e64^|lt_windows_10_consumer_editions_version_1903_x64_dvd_f5c0c33a^|https://x0.at/1_F^|2a16e04fc17458355c3334d36dafb1c54a7cc177
echo lv-lv^|2255e23a3f67f21391fe058d32c2daac74b70c4e^|lv_windows_10_consumer_editions_version_1903_x64_dvd_3438f521^|https://x0.at/8X1^|4ed4097aa8dafb2b9df6ba0f3619d65e62b91756
echo nb-no^|7688fd133602339608a895bed24a05e1c7ffef5a^|nb_windows_10_consumer_editions_version_1903_x64_dvd_6e07f9a8^|https://x0.at/Cvx^|c06c7d02254d32ba1e4bbad3adb337e877884866
echo nl-nl^|969628f29ac1943c6e478aa34a2018c4a731c122^|nl_windows_10_consumer_editions_version_1903_x64_dvd_898b90ee^|https://x0.at/hEs^|30769da4820330e5e9542d52cb6c871fd6ce79be
echo pl-pl^|b57e60ee38d7d545d800f2f026bffe9848a6f5a8^|pl_windows_10_consumer_editions_version_1903_x64_dvd_e974ed38^|https://x0.at/t25^|79e3854f34dc5b8ecc4e29c44aaa30cecdc7447c
echo pt-br^|209dde0c47be51eaefbc7466e72c5803da1a542b^|pt_windows_10_consumer_editions_version_1903_x64_dvd_1dcd7eab^|https://x0.at/_id^|5ccea0215846f01ff87e3f2cc34a7d77279f77d0
echo pt-pt^|833f6ec285042627f5be5214ebb1687bd800b1ac^|pp_windows_10_consumer_editions_version_1903_x64_dvd_110733cc^|https://x0.at/6h7^|6cf3ea9d1c6d9c029080f8a7120feeed6d8be416
echo ro-ro^|1848e545dad92c88a4cbbd677afea468ca7dcfc6^|ro_windows_10_consumer_editions_version_1903_x64_dvd_920b718c^|https://x0.at/aDZ^|a5936695ebf6965361593cce9f2af0b7cda2efaf
echo ru-ru^|7c58bd350161fc29596f070c0dc69694be7b7e7e^|ru_windows_10_consumer_editions_version_1903_x64_dvd_af20974d^|https://x0.at/jzh^|7f164eb8376e86972036db987556f7c915f7eda3
echo sr-latn-rs^|04a2ff2e0f9352854e79397a3dc7fa2230e3837c^|sr_windows_10_consumer_editions_version_1903_x64_dvd_9e8ee0a1^|https://x0.at/A1f^|e468d4181b33c69d4c1890ebdcb9b53436156e2b
echo sk-sk^|d9e13cec1a7f21b01e020891138550d8ecf5b5c0^|sk_windows_10_consumer_editions_version_1903_x64_dvd_943ad1c8^|https://x0.at/ElH^|f48edbda84741a0883cd80cf147144015d15251b
echo sl-si^|0f95658f087266ad1ab664c269417a38762fa439^|sl_windows_10_consumer_editions_version_1903_x64_dvd_918f1dd9^|https://x0.at/wRt^|7f3d64fe6d807949a12aeddc4753556355942f03
echo sv-se^|ea07ecd15d5cc286199866743c924d58a5860509^|sv_windows_10_consumer_editions_version_1903_x64_dvd_ee64a21c^|https://x0.at/kEm^|52d3600853f6540b3069ee3396235534b6577b2a
echo th-th^|49a60d6492f86ae9a4c1c11706f769e6e7dbcd93^|th_windows_10_consumer_editions_version_1903_x64_dvd_3731b1d2^|https://x0.at/Lly^|7e3c2db2b97a4a884019da8e375e31cd385d2c27
echo tr-tr^|3057ab3d635775d8c8c0e557703570c22d123af3^|tr_windows_10_consumer_editions_version_1903_x64_dvd_d7b05403^|https://x0.at/xJr^|d1ffdc14da85fcd3f68c8e506fdc02693c9fa26e
echo uk-ua^|03e1ccc463eb59439988a09057998e5c87d4ab54^|uk_windows_10_consumer_editions_version_1903_x64_dvd_e1022cf0^|https://x0.at/Q0Y^|8415dd35dc75a73b6496089a542b87d4138d0313
echo zh-cn^|cf1e64128585b6ddf7cc53d91758d7b0f836b863^|cn_windows_10_consumer_editions_version_1903_x64_dvd_8f05241d^|https://x0.at/3_d^|e7b49afe13b7d5acf1f8e45d27b27a04bdb418b4
echo zh-tw^|719c7c26f81901c4dce658192ed81c45418b53ad^|ct_windows_10_consumer_editions_version_1903_x64_dvd_d11ed14b^|https://x0.at/LUO^|94dcf18c0e0c0008293c9a4b487dd8f9954015e8
echo ar-sa^|a1c9efc846d669d8150e83e47cfd134fd24a808c^|ar_windows_10_consumer_editions_version_1903_x86_dvd_c3cb2240^|https://x0.at/5kT^|65e1bd2bd411810878284f9a37970110afb09e0e
echo bg-bg^|8fb4fa5685a6be8486a09f1dc3c4ddb2970e8e2b^|bg_windows_10_consumer_editions_version_1903_x86_dvd_c8562035^|https://x0.at/06T^|56602dd2604c101ca57f9308c216748947da6943
echo cs-cz^|f925d45eb35a6faab84252386e650b34bcf371fa^|cs_windows_10_consumer_editions_version_1903_x86_dvd_a0422ef2^|https://x0.at/lrS^|b3777d01a91b5c85898d7cbb839176450a3deab6
echo da-dk^|9ea159f10241880a726924d06e0b93c80c162850^|da_windows_10_consumer_editions_version_1903_x86_dvd_2e22aa3e^|https://x0.at/Mlr^|13f0268a12d0cc19f1061b629b339af5c8bf46f8
echo de-de^|615d733e15b4c0b0c246dea8c8a11c30abf751f7^|de_windows_10_consumer_editions_version_1903_x86_dvd_ffcccc0d^|https://x0.at/1-7^|f55176ee214267ed871e87717916bcc9cff7a54f
echo el-gr^|3bb13e88afd4dd4175f385a9cc1ea8a09e246d5d^|el_windows_10_consumer_editions_version_1903_x86_dvd_df61dc51^|https://x0.at/YwK^|e11fe24b86529b462a5136c56d9d2a09aae51e7f
echo en-gb^|ff6fb948a01cd810d628e72d08a94ed28fa72822^|en-gb_windows_10_consumer_editions_version_1903_x86_dvd_edcb6966^|https://x0.at/4Zi^|7cdf3cbdb9c9a0c3fe58a06989d0cac3a4460086
echo en-us^|e9e187ce83a662619ed05b5b783c12f861e18682^|en_windows_10_consumer_editions_version_1903_x86_dvd_b40c5211^|https://x0.at/4Zi^|7cdf3cbdb9c9a0c3fe58a06989d0cac3a4460086
echo es-es^|07785485d7100ee63da3bea8f49936158a871439^|es_windows_10_consumer_editions_version_1903_x86_dvd_c3318dc3^|https://x0.at/1cW^|b2642398b8b4748bda27e3d67259f510b6af90ec
echo es-mx^|28c8c1aa0c4334db48159f3e21cb7056713191ed^|es-mx_windows_10_consumer_editions_version_1903_x86_dvd_da6539e8^|https://x0.at/dQt^|815ad8554c3fc4b80b35d2890bc99cd06c5c8f74
echo et-ee^|234f4a2637ed5efa944685baf1f565e7e13fd652^|et_windows_10_consumer_editions_version_1903_x86_dvd_4e499b49^|https://x0.at/IRj^|c7efd52b2f462f614118f4a208f8c82384d403c8
echo fi-fi^|b5606641640636c73fe3fda93e59cfffb884ac0a^|fi_windows_10_consumer_editions_version_1903_x86_dvd_87b79a8a^|https://x0.at/moh^|e851c69db4846c72bf356a78b107d481984d1fd1
echo fr-ca^|5489d1501394e7baa0ac3199d83266e8fea6a768^|fr-ca_windows_10_consumer_editions_version_1903_x86_dvd_fa23321d^|https://x0.at/8L0^|e6a0433a09592fb3d6d3e19ce5b6fbc63095ff5d
echo fr-fr^|1e5c9957d0aafc911ed4cb6a069234b0b6f63c3e^|fr_windows_10_consumer_editions_version_1903_x86_dvd_fb873249^|https://x0.at/Tqb^|1a1cc744b8ca6855d25b756d003f535b83e4f925
echo he-il^|a1a5a9f6ae42d28b12067b3d50fda667ca500877^|he_windows_10_consumer_editions_version_1903_x86_dvd_6eb0f5ef^|https://x0.at/Ic6^|3f055e8d7cdc4c37229589551234dd46c8dd65aa
echo hr-hr^|3e752279296aa1e435bb302d3792cf15ecbfec6d^|hr_windows_10_consumer_editions_version_1903_x86_dvd_c241838f^|https://x0.at/Rwt^|ec681f510ca3e2fb734617423f60fe7be64e60fb
echo hu-hu^|fab19d63555088f883843866dfc5aeee55041bfa^|hu_windows_10_consumer_editions_version_1903_x86_dvd_84cbbe50^|https://x0.at/7EV^|0443a9a51d113c919a917b6d5a7345b39c4568b8
echo it-it^|7b74f59d558b798685f969a31eda581aaa3283b7^|it_windows_10_consumer_editions_version_1903_x86_dvd_39a04510^|https://x0.at/bMU^|bd30fac754f9b9adc46f89c553ee69be9ef3d64b
echo ja-jp^|0d2c3eb5058575f4945b62c1157f5e91028e59e3^|ja_windows_10_consumer_editions_version_1903_x86_dvd_883d5e02^|https://x0.at/gYu^|fa1a31950ee9572c23ecd7f5ad73c6ed44624039
echo ko-kr^|f71197acfc4825e51f1b3e8b84b8d270f03505b1^|ko_windows_10_consumer_editions_version_1903_x86_dvd_541add11^|https://x0.at/11g^|6f36cdf912c27cf46fe61b489fdb2b9af0dcd5ae
echo lt-lt^|573a9554b5a9eecd1c65ea66069104da47821c20^|lt_windows_10_consumer_editions_version_1903_x86_dvd_17d687a2^|https://x0.at/GhO^|c4275e90731e0f88bf18e56d81d147efc8655375
echo lv-lv^|f29f1d33217757616c350fd908799d37cdab22c6^|lv_windows_10_consumer_editions_version_1903_x86_dvd_ef5dc8ec^|https://x0.at/PV1^|7204dca4e2c40facbf677dfe7acb27fe9e7a3654
echo nb-no^|d01ce961ca4637f7b81cd9c3289887786342cfa1^|nb_windows_10_consumer_editions_version_1903_x86_dvd_23d8f834^|https://x0.at/LiB^|75a9e1feeba736abaa399b1f974b4728173a08de
echo nl-nl^|7f8cc639b5eb812429e4fdfbe8e99052a4813980^|nl_windows_10_consumer_editions_version_1903_x86_dvd_f66bb1fa^|https://x0.at/p5k^|a615f12a6f70994554fe591bf50aa0d3b69a472a
echo pl-pl^|2fe48d4393a927fc9898b109c7d21a3d8f47909c^|pl_windows_10_consumer_editions_version_1903_x86_dvd_2d4baf81^|https://x0.at/jTB^|40f5f43c972e2bba77da0e54d5c8159e3017fb84
echo pt-br^|77772c346af2ae0bc824e25ba45fdce1e034c836^|pt_windows_10_consumer_editions_version_1903_x86_dvd_ec185164^|https://x0.at/NyN^|02649ee9d9b129309ae6a9cee4940544666f073f
echo pt-pt^|016147da7b77a8c7555b1cea442072c7840a5e06^|pp_windows_10_consumer_editions_version_1903_x86_dvd_5803cb6b^|https://x0.at/s-I^|496db7eb4e8403046b830e8d3ddc8a125f9d261d
echo ro-ro^|7ebd3edb114ea7f1715ac2dad9f0dcb39efb0c71^|ro_windows_10_consumer_editions_version_1903_x86_dvd_826cb758^|https://x0.at/n8V^|a28e88ad008ab5ec19c843a9bc99cf56e2a009b2
echo ru-ru^|a7133d6ec8974de125e83a0aa86824547fcbb8a4^|ru_windows_10_consumer_editions_version_1903_x86_dvd_c7a56b3d^|https://x0.at/VKZ^|72aef769f5e70f5e6f2866de4251048d0bac23a9
echo sr-latn-rs^|67af2d66a88eef179e5952d9598710c87a070892^|sr_windows_10_consumer_editions_version_1903_x86_dvd_8b0101a7^|https://x0.at/lA7^|587ca699cd1a0b1be025f697ef655024ce627bc5
echo sk-sk^|daeb528f203abf43d318725f62005cb16c2cd7bf^|sk_windows_10_consumer_editions_version_1903_x86_dvd_f420cf0f^|https://x0.at/xK-^|f24e01df765134403fbd729208920d250921c7e6
echo sl-si^|d388ab3775b2df74c73732173102d5cff2cdb8af^|sl_windows_10_consumer_editions_version_1903_x86_dvd_20473855^|https://x0.at/Yjh^|e92e1c6ea4624d0a6456a86b9f8927efb4cc168c
echo sv-se^|da55643bb0a01d672cb39f4598a70914603959d9^|sv_windows_10_consumer_editions_version_1903_x86_dvd_5d52a9d2^|https://x0.at/q7r^|b828b828f466e8b03fc08cb69ec346300c5c8ec5
echo th-th^|620e54fe7028ef1cba6432a4ab39629923129730^|th_windows_10_consumer_editions_version_1903_x86_dvd_5c7e3c11^|https://x0.at/oIW^|a7b3cde9e8323a2309d9111977e77bf5d5e3d6aa
echo tr-tr^|66603c87ab02e172c2817598d7d0e660a40b6951^|tr_windows_10_consumer_editions_version_1903_x86_dvd_b92bb452^|https://x0.at/3Z7^|a9711c1ecb25e8730e4216cd27d406898433b930
echo uk-ua^|5566d64c87e6d79cd1b62edb1d650b71ba11b701^|uk_windows_10_consumer_editions_version_1903_x86_dvd_4a85251e^|https://x0.at/0eq^|7f271ccf0e96a1daefcbd67273db79080196adc0
echo zh-cn^|7910d3b3ff7b836cf13924bf4a91bf51404b93d3^|cn_windows_10_consumer_editions_version_1903_x86_dvd_44b77216^|https://x0.at/sCY^|c0528fc9e7b5f85a13f11a37215acf34db3c338e
echo zh-tw^|8a1854827f34ebe559e01c5a884e892619a25d8c^|ct_windows_10_consumer_editions_version_1903_x86_dvd_ca41a27f^|https://x0.at/oE6^|66515a4979159dc7960bab49ccbdee910308ac12
echo ar-sa^|3dbddaec141b45bfeead967f83580ca8e69ed40c^|ar_windows_10_business_editions_version_1903_x64_dvd_c97d194d^|https://x0.at/lqx^|bb532018b096bb97a91dc8c14b5c03799b6728ab
echo bg-bg^|dbf5b15a04f9e1344939f11b3e7554c7aac02b7a^|bg_windows_10_business_editions_version_1903_x64_dvd_d1a70906^|https://x0.at/G_8^|4a4f61e2f1707e0d2f3ce0bf5c39d8b93f6be070
echo cs-cz^|5291a67baaba121f0b92e1c780cae684dc34c428^|cs_windows_10_business_editions_version_1903_x64_dvd_dae335bd^|https://x0.at/hNQ^|81a6892ab478eb1d1b729a2fc7befedbfe2208ca
echo da-dk^|c3010f17d96eb4252d0708abbb4d1e2a9b55eb5b^|da_windows_10_business_editions_version_1903_x64_dvd_f1cc79d3^|https://x0.at/Ney^|a33f066ea5c9203714ac1cc091f502ad73d0e344
echo de-de^|5fec7c1686023547165d55dbbd4161d6533d8f44^|de_windows_10_business_editions_version_1903_x64_dvd_fd8d94a0^|https://x0.at/UeS^|49fedd8c829ee3dd876592f7d846330ea6475715
echo el-gr^|5e40813fe8030d72273d9e3a7515efc26b676422^|el_windows_10_business_editions_version_1903_x64_dvd_6b0800bc^|https://x0.at/orL^|2226ea97a8b0749c6183744fd494a5ba986eba2f
echo en-gb^|3df67db9f9e8f181644b0503dfb2fee385570e32^|en-gb_windows_10_business_editions_version_1903_x64_dvd_4170a06f^|https://x0.at/ELl^|ba3d34f977ead965034f3e0354b300f0640fe4e2
echo en-us^|dd9a1a27c5c58140b13facf6755a0c4a40ff0a6d^|en_windows_10_business_editions_version_1903_x64_dvd_37200948^|https://x0.at/QkN^|b8b16a1f411b1a83b645efc65c46457e744964aa
echo es-es^|b0d8dbdcd323be747ff2ae189f9f391bcd31ee5b^|es_windows_10_business_editions_version_1903_x64_dvd_8da6587a^|https://x0.at/GvF^|231eaab9293e877c2b6c361eb72b4cf6f93f54e6
echo es-mx^|d5d4bd775c75c0bfa5af50370d325600c641d847^|es-mx_windows_10_business_editions_version_1903_x64_dvd_9f9a2d26^|https://x0.at/hef^|e385e1f9cdc0ec4355052707c5b04ea0dc7064bd
echo et-ee^|50bf0e6152717ee1f1a2238d84fc678b868ceb83^|et_windows_10_business_editions_version_1903_x64_dvd_2e87ff26^|https://x0.at/dVa^|85fd80cd5123ed6755cbba530ab9d404368c6224
echo fi-fi^|5bde9c0b38e3219a9d05ecbc502293e9f2df8907^|fi_windows_10_business_editions_version_1903_x64_dvd_8a6e1b96^|https://x0.at/k6y^|1f841ecafafc26cd1515fb1d116fe62de986c1ea
echo fr-ca^|35788307a7efc1ebe8e02659dddba91f7387327d^|fr-ca_windows_10_business_editions_version_1903_x64_dvd_d698ccd4^|https://x0.at/1MP^|81acd71695cc263b8c26572717379d340c2bf00f
echo fr-fr^|888a12a692d1c30d728bfa2f007c660f866c6e8a^|fr_windows_10_business_editions_version_1903_x64_dvd_2cd2c3a6^|https://x0.at/zZ6^|ae304a24515441c8be1c80e855e883a265b4215f
echo he-il^|a73b426f23b24e8f4786aebfbffefe439c29e3c8^|he_windows_10_business_editions_version_1903_x64_dvd_3e807932^|https://x0.at/kgq^|994b9612fc5d35abaf0b60c334b42b08f469822d
echo hr-hr^|3630137b927187979386c71979b37ba8b441940c^|hr_windows_10_business_editions_version_1903_x64_dvd_21f512e8^|https://x0.at/z1C^|1d784b68bfb43b7c594a7a1b0b519ac31343fb14
echo hu-hu^|dc36a3ecb2bf1535bb882205a89adb34dfd40f3c^|hu_windows_10_business_editions_version_1903_x64_dvd_66227030^|https://x0.at/D06^|42ae487610ed462c00ef715f6ee122b25ae17c16
echo it-it^|ee75a54678665606a5f5de5f627101ff5bf87eab^|it_windows_10_business_editions_version_1903_x64_dvd_80006e02^|https://x0.at/R8P^|e36195022c9925692ea63e829fc593454d22df87
echo ja-jp^|360d4b6d5ae1183197633813fe8f485a01daf68e^|ja_windows_10_business_editions_version_1903_x64_dvd_3feb2933^|https://x0.at/PVA^|e8721a7af4a917f82f07bed262aa06aaba0c94d2
echo ko-kr^|022041e8f38247a42a3eaef9bcc569359e168efb^|ko_windows_10_business_editions_version_1903_x64_dvd_f2f1c827^|https://x0.at/ozX^|16803ebbde164eb7267ba745a66039c9a47f7c40
echo lt-lt^|aa5256c8d133838acb2dfc981c160f1525082854^|lt_windows_10_business_editions_version_1903_x64_dvd_db100a5c^|https://x0.at/UrE^|6f276fc7e9bdf20866507648c333900b504f01d1
echo lv-lv^|1ff29a9fddd52a133a4460b1627074a6ec5eb4db^|lv_windows_10_business_editions_version_1903_x64_dvd_63ebd35c^|https://x0.at/Ffa^|b56fd81bfc5dee55529ef3306afb0322ec9b7d24
echo nb-no^|9cb88bc296f943b68d7dece6e11d266117c7bf62^|nb_windows_10_business_editions_version_1903_x64_dvd_8807d26f^|https://x0.at/ywI^|0d27cd073a943edc80f529c1dca6f7ff67281c12
echo nl-nl^|28995a4033932bc8d4557a1a6a2be153a509b8a9^|nl_windows_10_business_editions_version_1903_x64_dvd_5b246428^|https://x0.at/Tc5^|79d5f1e41aa02dc3d7b4d8b71eb8082b1f77ac9a
echo pl-pl^|dcbe2e7419d6a8b0234354f115fd83b0778e7911^|pl_windows_10_business_editions_version_1903_x64_dvd_30ed9a97^|https://x0.at/tZP^|c2ff1430a1ce69937539cd13327245086a8e1cae
echo pt-br^|c0a3291bd4c921ec2bf7ed1401ae96d516bcac1c^|pt_windows_10_business_editions_version_1903_x64_dvd_15279e77^|https://x0.at/Hos^|093f7f8aefd3e677f5dd6428764c80da688e5774
echo pt-pt^|772f7bd07151a8e3fda4073cda692994ed86e57c^|pp_windows_10_business_editions_version_1903_x64_dvd_65265f10^|https://x0.at/jvR^|49148264bbe358d5010139594d4e5d8c16e76704
echo ro-ro^|4d1d5fb2d4dbd93fe710cd6ffda77a680c87fc84^|ro_windows_10_business_editions_version_1903_x64_dvd_86cdfa5e^|https://x0.at/9aK^|a624de3536e3ec81de66ad875fe33f1d54713ee4
echo ru-ru^|eae116a39d7a5166fe448342045529aa3e354cb3^|ru_windows_10_business_editions_version_1903_x64_dvd_8dbb0a6c^|https://x0.at/exq^|7d171d576a4feec6cb42208d5c4bf9278dda9f9f
echo sr-latn-rs^|64a3933aaeae06c5441603f8042a4c69b5a0129a^|sr_windows_10_business_editions_version_1903_x64_dvd_4330ea6a^|https://x0.at/VvD^|a54ef7f0a1283df2edb52a8bf90431071f0259d4
echo sk-sk^|eae445d87cae86ebc33acf063ae8c34a0510287b^|sk_windows_10_business_editions_version_1903_x64_dvd_ec6c5734^|https://x0.at/3fN^|1dc8b870d273bb9813d680b27a1bade1ceeaa105
echo sl-si^|da8e331f6ab2a568bc0b024cebe0a064e6dcc56f^|sl_windows_10_business_editions_version_1903_x64_dvd_49f000d1^|https://x0.at/Uv0^|dd32d05d15212dae70fbb1ce2dee75e7a2efdbda
echo sv-se^|71f8189e10e29318a32e7d93f61b37b66f51b1fa^|sv_windows_10_business_editions_version_1903_x64_dvd_067a22ec^|https://x0.at/KvH^|e2c7afabc5895a1c34d48ec19cec9fe1ab2fadab
echo th-th^|6c66c8edac8d0565bfe55768d758651d66dfc127^|th_windows_10_business_editions_version_1903_x64_dvd_91de592d^|https://x0.at/oFa^|214c4a1ef93eedf82ca1778341c6c57a18a52e93
echo tr-tr^|b3724ae597fb34075a62fe8ef5060fec07d5075c^|tr_windows_10_business_editions_version_1903_x64_dvd_189f71fd^|https://x0.at/sNd^|03e94789574d173ac959fa13f6359223fb55f7ab
echo uk-ua^|2f56566d8fbfc964d95a3e683d23a83c59f5bb4f^|uk_windows_10_business_editions_version_1903_x64_dvd_b7b37524^|https://x0.at/u_W^|a192c822bf65dedbb4650857b5376735d3a37a9f
echo zh-cn^|bc6176bee6130446421aca236e7944ee88c92fe8^|cn_windows_10_business_editions_version_1903_x64_dvd_e001dd2c^|https://x0.at/rWQ^|b2a32c74a32c70915ed2393e89b0198e2ec62612
echo zh-tw^|017fa41ee0b9c5061729bf8b58cf0f817635a81e^|ct_windows_10_business_editions_version_1903_x64_dvd_4ce47064^|https://x0.at/PGK^|06e971ce2bb4f2af8b1404395430c99183b2f5d8
echo ar-sa^|2d7d1807ceb721f5d15605f6acb575610b6dabca^|ar_windows_10_business_editions_version_1903_x86_dvd_c0ec1a55^|https://x0.at/9xP^|8e34b2d2b4bda1a908aab03c0357a25b8c25f809
echo bg-bg^|51386705469a174200338f761f7164922333bef8^|bg_windows_10_business_editions_version_1903_x86_dvd_9ae1c8a0^|https://x0.at/usU^|5968e1e2f8d2349c379d8f84570bf66eecaa157e
echo cs-cz^|bb922edb034e76855332b522b2f614cfb5991c88^|cs_windows_10_business_editions_version_1903_x86_dvd_955b8824^|https://x0.at/yeJ^|92784ebc9a91c8335dc68b39f769b463edb0e1fa
echo da-dk^|d0efec1ee33aa3a16c26673a5cbbf8f8ef152b80^|da_windows_10_business_editions_version_1903_x86_dvd_1e0ba544^|https://x0.at/x2Y^|a7034df17200469f73304d2a0b72bb0b1e73b4af
echo de-de^|42deb7d0488d85b0a2c13adabee27a5de6d973b7^|de_windows_10_business_editions_version_1903_x86_dvd_913365d1^|https://x0.at/t5W^|07f522a9a4855a69cc9a4a7a017b4c13400ab99b
echo el-gr^|e73f9dd33278889daef2e1218444eff007241e69^|el_windows_10_business_editions_version_1903_x86_dvd_8c1004bf^|https://x0.at/Wc-^|86d2b13053dbb8ba28830ccaaac0975c7c13b541
echo en-gb^|77a06649f1a58bb5bfc15c870f95c7720d73e540^|en-gb_windows_10_business_editions_version_1903_x86_dvd_20f0e2d7^|https://x0.at/8dZ^|eb48941b43eba32e63c5147e9d75e83275cbe480
echo en-us^|dfe44d88849bedf9df686dfaa9964a9b1009986b^|en_windows_10_business_editions_version_1903_x86_dvd_ca4f0f49^|https://x0.at/m5A^|88df837201546b56c414742fc6f3c7dc333adec4
echo es-es^|5766ca68f88420cbf39e5110d94babfc72056313^|es_windows_10_business_editions_version_1903_x86_dvd_92038efb^|https://x0.at/39Q^|c15eefcb9ba9d3ac181dbd3426dd7b581ae0f00f
echo es-mx^|6c37e41e229eab1273c8bcc8d02724802e534e40^|es-mx_windows_10_business_editions_version_1903_x86_dvd_452a0c98^|https://x0.at/lht^|bd0e792375b1e5fc2ed776d36e5db5463581482e
echo et-ee^|7ce01b6a954f15e785c37305a593f65edc44a7f5^|et_windows_10_business_editions_version_1903_x86_dvd_03d89a16^|https://x0.at/-Wj^|15866784b14f585cea506eb719896041eed0daf5
echo fi-fi^|0da48f890a6f7336bf1805ecf90af64fac87cdf2^|fi_windows_10_business_editions_version_1903_x86_dvd_74e9c1a9^|https://x0.at/SpS^|5ecf381d3e318b8b530235a702c668a114f4eb28
echo fr-ca^|839a427be637a246168179d3061dea9f6aa0a552^|fr-ca_windows_10_business_editions_version_1903_x86_dvd_37b0794c^|https://x0.at/l8h^|f351c2c15ee84517746b8cbc4808a5bd3c6a4564
echo fr-fr^|a5e169ed55e3aaaf9c4104d20238afe1e6890eac^|fr_windows_10_business_editions_version_1903_x86_dvd_d51186e3^|https://x0.at/tYt^|e9238bff3e0681f0ae0f3095b23d1524f31002c4
echo he-il^|43f7d9c7b70418348c6d60af8774eb263eee68d7^|he_windows_10_business_editions_version_1903_x86_dvd_c8030db9^|https://x0.at/Hkc^|91263be1c3919b0000bef4201540b2fc3cb995c9
echo hr-hr^|c0cfd56d0cb17edfc8b7bff2ebe9be464173c73d^|hr_windows_10_business_editions_version_1903_x86_dvd_d0560cd0^|https://x0.at/yp5^|42edf828cb0b16acd89e61d8d1dafb4b7fcb3695
echo hu-hu^|be3e0bf5ad70542ad6af24ae69bfd5f54c97a4df^|hu_windows_10_business_editions_version_1903_x86_dvd_554c5f27^|https://x0.at/ocA^|6c1c823adedfff916516163e4b0629d367239437
echo it-it^|c16ca5546493332904ce8330a90cc0e5edfe8cda^|it_windows_10_business_editions_version_1903_x86_dvd_fbbd1f98^|https://x0.at/2X0^|2c639c279dbfa556ae831eb15d32233894d07e78
echo ja-jp^|d66bce5744fddda54e0cf519a5c27a43dd327860^|ja_windows_10_business_editions_version_1903_x86_dvd_82f3ee4d^|https://x0.at/Y9z^|fffcfbb384e0ea6b045ff73a72a785c0e290fb26
echo ko-kr^|9637a28fe042b31473dcc46c7a6b8f1ee03c94ee^|ko_windows_10_business_editions_version_1903_x86_dvd_591f36d4^|https://x0.at/38y^|9b6bed7441d615f96c9f013660396420b5043e8e
echo lt-lt^|8b53d9d65d6404a5417c22150071ede259a82f45^|lt_windows_10_business_editions_version_1903_x86_dvd_3419d92f^|https://x0.at/NlS^|2bd1b88e976af59a39e844704f36440c479acff2
echo lv-lv^|5cfccd63c6718afa3ec0810b39a8eb7ce076121f^|lv_windows_10_business_editions_version_1903_x86_dvd_c8ed3931^|https://x0.at/KyX^|3d6297976538c212ce8e23ebe52379e903c757d1
echo nb-no^|964a91060689ca37bda6a1ea8a70a7ccf462eed3^|nb_windows_10_business_editions_version_1903_x86_dvd_93374720^|https://x0.at/KJd^|3ce61999a228ccec888ffc1bad10594e6f97953b
echo nl-nl^|1a17a54459a825c9f5742882ee670a1307c0516c^|nl_windows_10_business_editions_version_1903_x86_dvd_45035d30^|https://x0.at/pOT^|7678caef1aa86596ecef803286b643948ed27ac6
echo pl-pl^|1ec065c501670ae7a49bd79cb7fbd3819d2d6da2^|pl_windows_10_business_editions_version_1903_x86_dvd_53bc714f^|https://x0.at/w4O^|c0e69b69d2a2dc3f39ac8af5bb22b90bf4a94a03
echo pt-br^|a3876e89c97d6ca66e9fa1282b03ef1d13f9b1b6^|pt_windows_10_business_editions_version_1903_x86_dvd_7cc673fd^|https://x0.at/I8W^|58e5d42135c607fa8dcb18c2bcf3ca6f87c39919
echo pt-pt^|1255788b5598789467ead9e5549503081bc615c2^|pp_windows_10_business_editions_version_1903_x86_dvd_ee1826ea^|https://x0.at/FGO^|616ea238d87e1111a5a2a78b9a0f2771ddfae573
echo ro-ro^|615c5864a928d73b8ed48dfe890eca56bb927682^|ro_windows_10_business_editions_version_1903_x86_dvd_cfafdbd9^|https://x0.at/huX^|158d7d5b93e9deec8ed72275d32eb67a718ea2cb
echo ru-ru^|a037e323f6c07b556f26744909d1b0eef0c988ae^|ru_windows_10_business_editions_version_1903_x86_dvd_1b5e2088^|https://x0.at/bo0^|b0a6bc5065cad36a4f73370577438bc48e512b67
echo sr-latn-rs^|ad3b6dd3e1ad1b2a51f5c3dd1605817475265533^|sr_windows_10_business_editions_version_1903_x86_dvd_359da0f3^|https://x0.at/HYY^|7c30ffd3716c0a271c1ea3969d3f7123d274a3d9
echo sk-sk^|bc8fc59ead53a8a1afb4e7e50567ef3c42441576^|sk_windows_10_business_editions_version_1903_x86_dvd_cf44a8ee^|https://x0.at/9U6^|904bb5c2bcef46045a8663c79bd172524d1f277d
echo sl-si^|977d33e849291fc8a46cf04e9edc7cf1d6af48bc^|sl_windows_10_business_editions_version_1903_x86_dvd_be2db4cb^|https://x0.at/rnf^|1b721e93c00b27a69d63482fc0f9c01c2ae1425e
echo sv-se^|1e6797ed579e28a5babbf862b33fc42ad36ddbd2^|sv_windows_10_business_editions_version_1903_x86_dvd_bfe23529^|https://x0.at/_v-^|cf4bdf2200361b5e3a180fbba076dfe8a6d9a3e0
echo th-th^|4fc6896443eeb555482a9fbd5f6d6b9ca86ccc46^|th_windows_10_business_editions_version_1903_x86_dvd_89e1eeca^|https://x0.at/jvF^|88aff76bdc6a231eda763b515e68db59071c4d3a
echo tr-tr^|0b6e6fb1d4a317ff7306740560d66d9afc253c2f^|tr_windows_10_business_editions_version_1903_x86_dvd_6fd5cf4c^|https://x0.at/T3d^|33ffaa4b3e63393b79e60d8b7cb34e1366b0119a
echo uk-ua^|f2f3d25891e4617a551fcdbedf0b89105bc80424^|uk_windows_10_business_editions_version_1903_x86_dvd_d8335928^|https://x0.at/zJJ^|9d8cc520aa6a33afac7afbd12c9ff7fcb05dd6df
echo zh-cn^|f4ac6ecf4bd4189277dbcca008e4d0ade79ecee8^|cn_windows_10_business_editions_version_1903_x86_dvd_645a847f^|https://x0.at/p8g^|4c2cc390a59e173dfe40ce0ec80c3aa42bba013b
echo zh-tw^|6b156532c1b768a11868a1684d794f74cda34b20^|ct_windows_10_business_editions_version_1903_x86_dvd_c91969d6^|https://x0.at/jUl^|a17cf5f175cf0204eda03aa39991a5684177577b
)
goto:eof
:================================================================================================================
:================================================================================================================
:================================================================================================================
:================================================================================================================
:================================================================================================================
::DBLTSC19 STREAMLINED
::===============================================================================================================
::===============================================================================================================
:DBLTSC19
>>database.LTSC19.smrt (
echo ar-sa^|d3ce6d62e1ef8f06240acac85929a5b91b45a454^|ar_windows_10_enterprise_ltsc_2019_x64_dvd_3e37e487^|https://x0.at/DM9^|bc9e3b3a4c8dd8e0f42bf5e510eb0dad7150c751
echo bg-bg^|f73d95f486c63d8486482ca34da9cf6766a0a61f^|bg_windows_10_enterprise_ltsc_2019_x64_dvd_d36fcb33^|https://x0.at/zw8^|181d7db8697e7e327b3ddfed5a227bbbef95167a
echo cs-cz^|92293aeecf652d97e5060edcb7ae41f6bc3fa4c9^|cs_windows_10_enterprise_ltsc_2019_x64_dvd_e323fd84^|https://x0.at/cE-^|27e1452087381695da4e4744372107c1bd6ec7da
echo da-dk^|ca38ff0f8320d4a3fabb39b433fb1de08779df38^|da_windows_10_enterprise_ltsc_2019_x64_dvd_6d5cd451^|https://x0.at/7d9^|ec6c5b10b264b494e88ff0cbc4ab895dc60ad6c2
echo de-de^|98191710cf349f544b7f3802f50845188dff951a^|de_windows_10_enterprise_ltsc_2019_x64_dvd_02e3955a^|https://x0.at/mDw^|4cf56be198fafe593e92c4c5d3003c86401fe021
echo el-gr^|4c90a87553d8efeb26dfe07d620985d7601db2a6^|el_windows_10_enterprise_ltsc_2019_x64_dvd_d5a29846^|https://x0.at/bSc^|cbfea397efa359bb7105b1d315c2eb6e7a2c43be
echo en-gb^|c6abd34f04a51393825b6c700179c0eed61c944a^|en-gb_windows_10_enterprise_ltsc_2019_x64_dvd_cd49b901^|https://x0.at/cdc^|db0b81671c06b068b6f501680f5ee30ededdf2b1
echo en-us^|0b8476eff31f957590ade6fe671f16161037d3f6^|en_windows_10_enterprise_ltsc_2019_x64_dvd_74865958^|https://x0.at/XMG^|e5b1732cb6dc3d930f8a7bcfee2ed127106f8d68
echo es-es^|cb68e9a22d2c79c44387ea94d7e7bedb3e997ce7^|es_windows_10_enterprise_ltsc_2019_x64_dvd_e94f898f^|https://x0.at/I37^|6de0019429a1c12f9b52e711babc2b5a0b707edd
echo es-mx^|5985bdf386eeb2f0216fa768ab9dc3bde3077395^|es-mx_windows_10_enterprise_ltsc_2019_x64_dvd_6af93e60^|https://x0.at/zJx^|d306e0354d177b9514b2d3ead8e05f3f0c00de9f
echo et-ee^|cea2d08d438a65aaf21de0176c04fda20cd7a027^|et_windows_10_enterprise_ltsc_2019_x64_dvd_6207d722^|https://x0.at/XZ3^|4f53143a7cbd4f3bca0e40c2ea65f6e4207119ba
echo fi-fi^|caf27a02e64f1a8ba7e8ff0c17064b658f064bf5^|fi_windows_10_enterprise_ltsc_2019_x64_dvd_a53ce04e^|https://x0.at/fhN^|1a5a0d645e2136c40f62677237959892a6452f5f
echo fr-ca^|8995b4fab8afc4b8a21c92e3301804d73203f1ae^|fr-ca_windows_10_enterprise_ltsc_2019_x64_dvd_80e7bb5a^|https://x0.at/4PF^|fb114bd4c091d7a08b9b63edba3d8aa01da0e4e3
echo fr-fr^|3253d69f61b97482737e23c2d41acf387cd5a03a^|fr_windows_10_enterprise_ltsc_2019_x64_dvd_5fe6b360^|https://x0.at/xHq^|9d7d47013a6198a441e98105738666d73caed67d
echo he-il^|a54ef55fa92b034d492bc580d13750f7b2a43654^|he_windows_10_enterprise_ltsc_2019_x64_dvd_2b9a413b^|https://x0.at/m0f^|04660e0732cf8db678854a93b49eca8d7dc6229c
echo hr-hr^|0d480bd8ba785688dbab3342abcbe98e98612b0b^|hr_windows_10_enterprise_ltsc_2019_x64_dvd_ab5beb9a^|https://x0.at/Gjq^|d153410b0adc737b650c0f32b79dda3034c23cde
echo hu-hu^|d62eb63883babd31d89d7213ff59834247349307^|hu_windows_10_enterprise_ltsc_2019_x64_dvd_134fe4a3^|https://x0.at/5wV^|0aa44ecf8b8070473a9962e6723500449f0571e6
echo it-it^|dafe5ea805a8cad94cbf36ede728b01fcfe6bd8b^|it_windows_10_enterprise_ltsc_2019_x64_dvd_2fa02691^|https://x0.at/h5L^|6d22f7871dcec3991fbe89869fb1e093281221c0
echo ja-jp^|6d17c2be54f91399865fd1d996084dee949fc645^|ja_windows_10_enterprise_ltsc_2019_x64_dvd_df74d1a7^|https://x0.at/9zv^|4f75f57ff568cc0fb2ffb066f53c436ecce2aa05
echo ko-kr^|86dfbd74c2f73e68f89f801c9d89fe3e42585c31^|ko_windows_10_enterprise_ltsc_2019_x64_dvd_09f5a038^|https://x0.at/Wgw^|7a695d0f607245693d5d21a549ecfbd4c05b9dd5
echo lt-lt^|323193c233f9cf433e744bfb755853f50c98f771^|lt_windows_10_enterprise_ltsc_2019_x64_dvd_7d317b48^|https://x0.at/5h9^|a128c010ef916bc32348c04280e27ac5364ae047
echo lv-lv^|ab4035ba9f9364bce287fb065157bfbda6e02858^|lv_windows_10_enterprise_ltsc_2019_x64_dvd_46018f3f^|https://x0.at/SfC^|2d0a44640b2d7f7f96fe95c0b9342470f3e3b3ff
echo nb-no^|96cb8da62ab178cfe3d4b2783771fb81b4abc9f8^|nb_windows_10_enterprise_ltsc_2019_x64_dvd_cbf12aac^|https://x0.at/O6Z^|75a498747d5d54f1b0b3f6986d1543a53f089ace
echo nl-nl^|92c4de2e5be51de1ea8437177c122a0a558d999c^|nl_windows_10_enterprise_ltsc_2019_x64_dvd_031a2076^|https://x0.at/trs^|42b16c1901ae1fc932a8ff003bd28eb463f4e162
echo pl-pl^|b036e0a1208507bf00d073adea01a9fa6740080c^|pl_windows_10_enterprise_ltsc_2019_x64_dvd_95779877^|https://x0.at/st6^|a653606e317d1d55ef13be07de959b4c2d2be27b
echo pt-br^|30a32617678161d1f6a57b48ad4e5d42b2a07928^|pt_windows_10_enterprise_ltsc_2019_x64_dvd_3e7f778a^|https://x0.at/ASo^|bcfd788140d5e24c58cf5e8b573e15e2575c8011
echo pt-pt^|624cef3a8ea1fffaf35e501fec2aa79556278b08^|pp_windows_10_enterprise_ltsc_2019_x64_dvd_888e368c^|https://x0.at/DNA^|e770a01452e85eef9d1fcfb35525eb53ca534457
echo ro-ro^|9618d8329fd5fac2696dd53bd1a3fc5711410c3f^|ro_windows_10_enterprise_ltsc_2019_x64_dvd_f2192606^|https://x0.at/CBt^|15dfc4ad563a6f877db4e4cf5992b4bfdb655526
echo ru-ru^|e31b7ca70d32d5df1900f4ea0b66d837cd374763^|ru_windows_10_enterprise_ltsc_2019_x64_dvd_9aef5d46^|https://x0.at/p_z^|27619ad1fb144b96324f8d82c457005817bc3ebf
echo sr-latn-rs^|98f3230bba41134e15743a7c536e84322cda2ab6^|sr-latn_windows_10_enterprise_ltsc_2019_x64_dvd_655d5080^|https://x0.at/vFa^|41491bdf44c9636705f7b0f167a9ad6c3b28317b
echo sk-sk^|1be1bef93c57d14a10581ea22e45cd617a7fd9eb^|sk_windows_10_enterprise_ltsc_2019_x64_dvd_1e7b6894^|https://x0.at/pPQ^|36ba5e5b7657e7d2e34f93f6e160b54a3effdbd4
echo sl-si^|0779ad4c923e0c4a5573dd74f7323c9fd403b386^|sl_windows_10_enterprise_ltsc_2019_x64_dvd_a8159150^|https://x0.at/w0w^|20b8c5acb9ee7b85618cbf4e90ff5390f047b329
echo sv-se^|322b064fba7f1511796d23b81a656a0f58ca7020^|sv_windows_10_enterprise_ltsc_2019_x64_dvd_df8faa90^|https://x0.at/hgP^|744957b542696cdcd44290e3a8c81f8e6933fa1a
echo th-th^|628f48827c3a50a757b5a086229b3a72ef207f77^|th_windows_10_enterprise_ltsc_2019_x64_dvd_c0300ce5^|https://x0.at/Z-H^|30aecddcebd5e1b2072af927bcdbef4eb0d9d0f8
echo tr-tr^|bb4a2e04e875ae66da8826f8e230ccec0f3af242^|tr_windows_10_enterprise_ltsc_2019_x64_dvd_cdd54ad3^|https://x0.at/Tkw^|a9ab9939ea71b5e199cfd65b1d14d161ebe38044
echo uk-ua^|9d2f69dd2a9497dade058676f0c305fee6123ae2^|uk_windows_10_enterprise_ltsc_2019_x64_dvd_ba0f310a^|https://x0.at/kLe^|7a1906728a6e672de4e4c5371b1fdb6eefe6a57f
echo zh-cn^|e04252542aeb494464ad9231f4f2e2c021eab2ec^|cn_windows_10_enterprise_ltsc_2019_x64_dvd_2efc9ac2^|https://x0.at/cxZ^|98487744eaa766a5395cfe871e8f4fedf25a55ce
echo zh-tw^|bb8495178de9f8fea2170ca891f40c02ef126049^|ct_windows_10_enterprise_ltsc_2019_x64_dvd_8f63a5f8^|https://x0.at/FGh^|ded1a86850591c2f2df2a70a5ef6ec27cd24077e
echo ar-sa^|b1d40e561924f78d2fb78044b2bf71cf2dd0ea7e^|ar_windows_10_enterprise_ltsc_2019_x86_dvd_c5368318^|https://x0.at/o3W^|b9beda996f75092ab21a5fe898445c4e03ab535c
echo bg-bg^|136226cdc8d6f34015bb41d15d93222ccb2a217c^|bg_windows_10_enterprise_ltsc_2019_x86_dvd_7e15939e^|https://x0.at/Jfp^|740fa2cb207930929f9406a4db48c54c633753ad
echo cs-cz^|7d2b6771d8a379bbb861118b3feac5a6d8c8ce31^|cs_windows_10_enterprise_ltsc_2019_x86_dvd_4b80f828^|https://x0.at/fj6^|39e1b59dde29473f285f8ce59aaa9d2e2edf9057
echo da-dk^|86850cff9c0daa205314cb3304428efec239c06d^|da_windows_10_enterprise_ltsc_2019_x86_dvd_0ec4b329^|https://x0.at/m54^|d929441511b917a16cf90c7b0018045d465d950c
echo de-de^|210ab0749d144562ec4dc3fc907578439fc9a7f6^|de_windows_10_enterprise_ltsc_2019_x86_dvd_000e9a1b^|https://x0.at/52z^|cb68b64c794b9ce61eac69bf5c847c4c24edc74b
echo el-gr^|2d6b6443e292e25d45cd750a0502be3b5fb54d6f^|el_windows_10_enterprise_ltsc_2019_x86_dvd_b7b4d78a^|https://x0.at/ItS^|1d11ce344c3c12004da2503abe86dd162fca00b5
echo en-gb^|0ceae4d33f0fa0328c2e49f3d8b553a3172156e6^|en-gb_windows_10_enterprise_ltsc_2019_x86_dvd_13a67815^|https://x0.at/pOV^|c5cf03d08d08acdfc9bb8b9747949de76e094044
echo en-us^|a935986f5a84af94fce423c0a6bdd2f743e5b92d^|en_windows_10_enterprise_ltsc_2019_x86_dvd_97449f83^|https://x0.at/YBC^|1174543485deb6c51386a0fd69e888c9c0f60fd9
echo es-es^|c81cb44ad55e88ac28057696cda6f6203afe6bad^|es_windows_10_enterprise_ltsc_2019_x86_dvd_b0f6388a^|https://x0.at/W_T^|6432ae33085be62613c84fc5e165f0a1ef0ef149
echo es-mx^|b427bbbc138068b76630fe8804ad65c03fade913^|es-mx_windows_10_enterprise_ltsc_2019_x86_dvd_55ef7eee^|https://x0.at/klV^|5235505ad1b5df37ba1cf393928ccbc98641e6b4
echo et-ee^|e4b67f600b57e2812a365b58be666c432ddf633f^|et_windows_10_enterprise_ltsc_2019_x86_dvd_4720d4e6^|https://x0.at/NB5^|36712e6569216f5d77a333e3a4360fc348971ba2
echo fi-fi^|3066c54b57ced428c685f26f3bc8119e0a4a8d39^|fi_windows_10_enterprise_ltsc_2019_x86_dvd_40841a40^|https://x0.at/dtX^|f1e30d11483a7174407e3e3864ac057063fd0080
echo fr-ca^|5ef037dcf38a6c42393d61d6e6926447e4d9047c^|fr-ca_windows_10_enterprise_ltsc_2019_x86_dvd_47e5f795^|https://x0.at/7r7^|d2836edd84d4db28f26d6971af0d89fd230f6686
echo fr-fr^|c7302ebd7778956242f979f823c6eb2c8fbd8d4c^|fr_windows_10_enterprise_ltsc_2019_x86_dvd_eb73ef44^|https://x0.at/2sM^|d2eddfc9cabf8855b12174aa7ca2408675e04d0c
echo he-il^|e7932d479485f288fe9ca5e1e8ca7ca82105bb7d^|he_windows_10_enterprise_ltsc_2019_x86_dvd_db900652^|https://x0.at/_w_^|f067796c952591913613db3a2a63101cd1c0a299
echo hr-hr^|3a1a3d359a52c04d0a7c8cb161d26e6d297a0e82^|hr_windows_10_enterprise_ltsc_2019_x86_dvd_59958d29^|https://x0.at/NGp^|683320a964ba6d5b5a489e6edaa997e87d8565fa
echo hu-hu^|58ce7d6c07d039bd853871b92ee677fa9c81d5bb^|hu_windows_10_enterprise_ltsc_2019_x86_dvd_6644ef51^|https://x0.at/kwE^|db9cf69c0ff59ad9bf2ef630f4e713d5ff4935e9
echo it-it^|522a77d6084a8066ba6297e4c664675dabe2adc6^|it_windows_10_enterprise_ltsc_2019_x86_dvd_12f58cc6^|https://x0.at/QLn^|7e204736e8be20c4712dad40f5c78a4acf4699cc
echo ja-jp^|6c302fe901b9c80d5cd78ba9469d705e7a225cde^|ja_windows_10_enterprise_ltsc_2019_x86_dvd_930f37b8^|https://x0.at/eVM^|afb527318abb241172b14a825c3732b651eb69d2
echo ko-kr^|d9a627a03510be84f2ed8544c6ceeea6a676eab9^|ko_windows_10_enterprise_ltsc_2019_x86_dvd_2906297d^|https://x0.at/cDb^|2cc86d66309fa7556200579b049ac9a3938c7a89
echo lt-lt^|96779b88fb24ad9480eebe45df2eb72400b51e27^|lt_windows_10_enterprise_ltsc_2019_x86_dvd_cebf1448^|https://x0.at/ip-^|3a5063ef3298e9a2435fe83033cf2c41d1cea144
echo lv-lv^|09d1b21389af7537d35005b3d375ee9e386c3415^|lv_windows_10_enterprise_ltsc_2019_x86_dvd_e00aeee5^|https://x0.at/3SG^|826e5a08de4fbf37607c644db1a5076a0d5f9377
echo nb-no^|db6a0f180fdd1edb91e5447a21432b2ed95c0c62^|nb_windows_10_enterprise_ltsc_2019_x86_dvd_f0a2bc17^|https://x0.at/-bE^|0018b842a33faee55509f9397d6842500e60b592
echo nl-nl^|2ad3ec3949d2be1f70a9f5a83fc158f502139789^|nl_windows_10_enterprise_ltsc_2019_x86_dvd_8e3f8670^|https://x0.at/FF3^|07e34d01f7c3ceb22fef9c467150e5439f607e93
echo pl-pl^|bddae3e8692e8d778d2a47db1da03e6047408ca6^|pl_windows_10_enterprise_ltsc_2019_x86_dvd_d95f58d5^|https://x0.at/lHY^|e80535e436a0718f5d64ce8bc30829f45b3ca6d4
echo pt-br^|a46ce93d4cf2b6abca96c69b42f8b6a1c90de95b^|pt_windows_10_enterprise_ltsc_2019_x86_dvd_193e121c^|https://x0.at/G4R^|658365042f6c241e0eb005dfd00180080bd4990e
echo pt-pt^|8f9e6d794fb851dcfc8e1bdf2a367408de279c73^|pp_windows_10_enterprise_ltsc_2019_x86_dvd_e20badc3^|https://x0.at/Zvk^|1e28e427c5383576f112f3daebc547a4c40c43cd
echo ro-ro^|a689ff8007d1150b7cf02ee1cc46be5af0387bdd^|ro_windows_10_enterprise_ltsc_2019_x86_dvd_442dd919^|https://x0.at/axW^|a61e169ed1dd020402680ca98c6e3bc3bc9de6ad
echo ru-ru^|909fe1bc41aeb93508fe4ecde34780a53f9b8bf3^|ru_windows_10_enterprise_ltsc_2019_x86_dvd_bbe85ffd^|https://x0.at/5D-^|c4bf4ff7b0d086286493da8a5f37c0605fd2055a
echo sr-latn-rs^|8372fdc646d4c7b13c3df23a88319e0fc0e75d9e^|sr-latn_windows_10_enterprise_ltsc_2019_x86_dvd_05d7f6f2^|https://x0.at/-OP^|c7158529c5190d5541420c2fa2cc706124a7e3fb
echo sk-sk^|95d2169e51cff1a3a4c3c83699c25072501dedc4^|sk_windows_10_enterprise_ltsc_2019_x86_dvd_c4969d7c^|https://x0.at/u9f^|051fbe468704a714d8beb3b79b468c8d44d9c407
echo sl-si^|8765095d05c9a07878dc521723368d7fa5cfc90e^|sl_windows_10_enterprise_ltsc_2019_x86_dvd_841f11ff^|https://x0.at/GLS^|f5f210ecbeebfa192b1bacc650528ff93918fc30
echo sv-se^|ac07d6a362d293fef13d463a10e7da510a2a0f40^|sv_windows_10_enterprise_ltsc_2019_x86_dvd_3d4776c0^|https://x0.at/U1w^|17f0152f481ff2946a0b3d2f77663a9f7e059e7b
echo th-th^|1fff348bccefd3f29631e956b5eb79d2e52340ef^|th_windows_10_enterprise_ltsc_2019_x86_dvd_7f6f3b85^|https://x0.at/dc7^|3f8a0d707906d367493f012a4a4c2811bc67b587
echo tr-tr^|716817fc7ef01f3992f9ef7100a6670dfb5e840c^|tr_windows_10_enterprise_ltsc_2019_x86_dvd_ad66b403^|https://x0.at/QxB^|98407f972150f8b4150770714e157c2a8bdc2ff8
echo uk-ua^|70c17b0310c9a294051d24b054c14fc837a3e4c5^|uk_windows_10_enterprise_ltsc_2019_x86_dvd_00360a79^|https://x0.at/vvG^|570a3cb22b801f795c74ae9736562992c73765b9
echo zh-cn^|37cd47e5b0e28ace85672dc6731b58fe7539f84b^|cn_windows_10_enterprise_ltsc_2019_x86_dvd_2908ee10^|https://x0.at/XuL^|500e39b1b32719676f2e5f49a72867c6e00a0184
echo zh-tw^|b74cd7b58dd51ae37278db88ced4cab5ac1facc4^|ct_windows_10_enterprise_ltsc_2019_x86_dvd_ca810a2b^|https://x0.at/8X8^|e250834770e9d7fafead1554773aab9d4df6d111
)
goto:eof
:================================================================================================================
:================================================================================================================
:================================================================================================================
:================================================================================================================
:================================================================================================================
::DBDownSh STREAMLINED
::===============================================================================================================
::===============================================================================================================
:DownShStream
>>download.sh (
echo #^^!/bin/bash
echo:
echo if [ -z "$1" ]; then
echo    echo "Please specify sharelink"
echo    exit
echo fi
echo:
echo share="$1"
echo fileName="$2"
echo:
echo link="https://pcorakel.stackstorage.com/s/$share"
echo dlLink="https://pcorakel.stackstorage.com/public-share/$share/download"
echo:
echo output=`./curl "$link" -D- -s`
echo token=`echo "$output" ^| sed 's/^>/^>\n/g' ^| grep -i CSRF-TOKEN ^| sed 's/.*content="//g;s/"^>//g'`
echo cookie=`echo "$output" ^| grep -i Set-Cookie ^| sed 's/Set-Cookie: /Cookie: /g;s/;.*//g'`
echo:
echo download="archive=zip&all=false&query=&CSRF-Token=$token&paths[]=/$fileName"
echo:
echo ./curl "$dlLink" -d "$download" -O -J -H"$cookie"
)
goto:eof
:================================================================================================================
:================================================================================================================
:================================================================================================================
:================================================================================================================
:================================================================================================================
::DBgetLinkSh STREAMLINED
::===============================================================================================================
::===============================================================================================================
:getLinkShStream
>>getLink.sh (
echo #^^!/bin/bash
echo:
echo if [ -z "$1" ]; then
echo    echo "Please specify product ID"
echo    exit
echo elif  [ -z "$2" ]; then
echo    echo "Please specify file name"
echo    exit
echo fi
echo:
echo function uuidGen {
echo    local tmp
echo    local i=0
echo    local string=
echo:
echo    while [ $i -lt 32 ]; do
echo        i=$^(^( i+1 ^)^)
echo        tmp=$^(^( $RANDOM%%16 ^)^)
echo:
echo        case $tmp in
echo        10^) tmp='a' ;;
echo        11^) tmp='b' ;;
echo        12^) tmp='c' ;;
echo        13^) tmp='d' ;;
echo        14^) tmp='e' ;;
echo        15^) tmp='f' ;;
echo        esac
echo:
echo        case $i in
echo        8^|12^|16^|20^) tmp=$tmp- ;;
echo        esac
echo:
echo        string=$string$tmp
echo    done
echo:
echo    printf "$string"
echo }
echo:
echo uuid=`uuidGen`
echo:
echo productId="$1"
echo fileName="$2"
echo:
echo getLangs="http://www.microsoft.com/en-us/api/controls/contentinclude/html?pageId=a8f8f489-4c7f-463a-9ca6-5cff94d8d041&host=www.microsoft.com&segments=software-download%%2cwindows10ISO&query=&action=getskuinformationbyproductedition&sessionId=$uuid&productEditionId=$productId&sdVersion=2"
echo getLink="https://www.microsoft.com/en-us/api/controls/contentinclude/html?pageId=160bb813-f54e-4e9f-bffc-38c6eb56e061&host=www.microsoft.com&segments=software-download%%2cwindows10ISO&query=&action=GetProductDownloadLinkForFriendlyFileName&sessionId=$uuid&friendlyFileName=$fileName&sdVersion=2"
echo postData="controlAttributeMapping="
echo errorText="We encountered a problem processing your request"
echo:
echo ./curl "$getLangs" -d"$postData" ^> /dev/null
echo output=`./curl "$getLink" -d"$postData" -s`
echo:
echo if ^(echo $output ^| grep "$errorText" ^> /dev/null^); then
echo    echo "ERROR"
echo    exit
echo fi
echo:
echo echo "$output" ^| grep -o 'http.*software-download.*"' | sed 's/",download.*//g'
)
goto:eof
::===============================================================================================================
::===============================================================================================================
::===============================================================================================================
::===============================================================================================================
:EOF